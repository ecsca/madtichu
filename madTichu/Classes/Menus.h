
#include "cocos2d.h"
//#include "login.h"
#include "network/HttpClient.h"
using namespace cocos2d::network;

class Menus : public cocos2d::LayerColor
{
public:
	static cocos2d::Scene* createScene(std::string rid);
	std::string roomnum;
	std::string rId;
	bool joined;

	int userNum;

	virtual bool init();

	CREATE_FUNC(Menus);
	void joinGame(cocos2d::Object* pSender);
	void doPushScene(HttpClient *sender, HttpResponse *response);
	void doPushSceneTran(cocos2d::Object* pSender);
	void doReplaceScene(cocos2d::Object* pSender);
	void doReplaceSceneTran(cocos2d::Object* pSender);
	void readyGame(HttpClient *sender, HttpResponse *response);
	cocos2d::TransitionScene* createTransition(int nIndex, float t, cocos2d::Scene* s);
	cocos2d::LabelTTF* _labelStatusCode; 
	cocos2d::Menu * pMenu;
	//cocos2d::LabelTTF* w;
};


