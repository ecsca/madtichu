#include "User.h"

class GameHelper{
private:
	Card* floor[MAXCARD][MAXCARD]; // 현재 바닥에 깔린 패
	int floorCnt;

	
	

	bool isBomb(Suit s[], int v[], int cnt);
	bool isConPair(Suit s[], int v[], int cnt);
	bool isStraight(Suit s[], int v[], int cnt);
	bool isFullHouse(Suit s[], int v[], int cnt);
	



public:
	bool submitCheck;
	int lastSubmit;


	int un;
	int callCard();
	User* users[USERNUM]; // user들s

	bool isAvailable(Card** hand);
	int nextTurn();

	int prevTurn;
	int curTurn; // 현재 user
	Combination curComb; // 현재 바닥에 깔린 조합
	int curFlNum; // 현재 바닥에 깔린 카드 수
	double curValue; // 바닥에 깔린 카드 값

	int passCnt; // 연속해서 pass를 외친 사람의 수
	bool prevPass;

	int userCnt;
	int score[4];

	bool passCall[4];

	bool canSubmit(int userNum);
	bool submit(int userNum);
	bool pass();
	void calculateScore(); // 미완성
	int getCurTurn();

	bool gameStart;
	int tichuCall;
	int Dra;
	int Maj;

	std::string toString();
	std::string toString_score();

	/*
	GameHelper(){
		int i = 0;

		floorCnt = 0;
		Card* card[MAXCARD];
		for (i = 1; i <= MAXHAND; i++) card[i - 1] = new Card(Suit::Diamonds, i);
		for (i = 1; i <= MAXHAND; i++) card[i + 13] = new Card(Suit::Clubs, i);
		for (i = 1; i <= MAXHAND; i++) card[i + 27] = new Card(Suit::Hearts, i);
		for (i = 1; i <= MAXHAND; i++) card[i + 41] = new Card(Suit::Spades, i);

		std::random_shuffle(card, card + MAXCARD);
		for (i = 0; i < MAXCARD; i++){
			printf("%d %d %d %d\n", i, card[i]->getValue(), card[i]->getSuit(), card[i]->getSubmit());
		}
		int index = 0;
		for (i = 0; i < USERNUM; i++){
			users[i] = new User(&card[index]);
			index += MAXHAND;
		}


		curTurn = 0;
		curComb = Combination::None;
		curFlNum = -1;
		curValue = 0;
		passCnt = 0;

		while (true){
			Card *carda[MAXHAND];
			memcpy(carda, users[curTurn]->getHand(), 4 * MAXHAND);
			printf("==============\n");
			for (i = 0; i < MAXHAND; i++){
				if (carda[i]->canUse()){
					printf("%d %d %d\n %d %d\n", i, carda[i]->getValue(), carda[i]->getSuit(), carda[i]->getSubmit(), carda[i]->canUse());
				}

			}
			printf("current turn : %d\n enter submit card count : ", curTurn);
			int cnt;
			int j;
			scanf("%d", &cnt);
			if (cnt == 0) pass();
			else{
				for (i = 0; i < cnt; i++){
					printf("choose card : ");
					scanf("%d", &j);
					carda[j]->setSubmit(true);
				}
				if (!submit(curTurn)){
					if (carda[i]->canUse() && carda[i]->getSubmit()){
						carda[i]->setSubmit(false);
					}
				};
			}
			printf("current turn : %d, curFLNUM : %d, curValue : %d, curComb: %d\n", curTurn, curFlNum, curValue, curComb);
			printf("passCnt: %d, floorCnt : %d\n", passCnt, floorCnt);
		}
	}*/
	Card * _card[MAXCARD];

	GameHelper(){
		
		srand(time(NULL));
		int index = 0, i;
		for (i = 1; i < MAXHAND; i++) _card[i - 1] = new Card(Diamonds, i);
		_card[MAXHAND - 1] = new Card(Special, -10);
		for (i = 1; i < MAXHAND; i++) _card[i + 13] = new Card(Clubs, i);
		_card[MAXHAND + 13] = new Card(Special, -20);
		for (i = 1; i < MAXHAND; i++) _card[i + 27] = new Card(Hearts, i);
		_card[MAXHAND + 27] = new Card(Special, -30);
		for (i = 1; i < MAXHAND; i++) _card[i + 41] = new Card(Spades, i);
		_card[MAXHAND + 41] = new Card(Special, -40);

		std::random_shuffle(_card, _card + MAXCARD);

		for (i = 0; i < USERNUM; i++){
			users[i] = new User(&_card[index]);
			index += MAXHAND;
		}

		curTurn = 0;
		prevTurn = 3;

		curComb = Combination::None;
		curFlNum = -1;
		curValue = 0;
		passCnt = 0;
		userCnt = USERNUM;
		gameStart = false;

		for (i = 0; i < USERNUM; i++){
			score[i] = 0;
		}

	}

	GameHelper(int t){
		srand(time(NULL));
		int index = 0, i;
		for (i = 1; i < MAXHAND; i++) _card[i - 1] = new Card(Diamonds, i);
		_card[MAXHAND - 1] = new Card(Special, -10);
		for (i = 1; i < MAXHAND; i++) _card[i + 13] = new Card(Clubs, i);
		_card[MAXHAND + 13] = new Card(Special, -20);
		for (i = 1; i < MAXHAND; i++) _card[i + 27] = new Card(Hearts, i);
		_card[MAXHAND + 27] = new Card(Special, -30);
		for (i = 1; i < MAXHAND; i++) _card[i + 41] = new Card(Spades, i);
		_card[MAXHAND + 41] = new Card(Special, -40);

		std::random_shuffle(_card, _card + MAXCARD);

		for (i = 0; i < USERNUM; i++){
			users[i] = new User(&_card[index]);
			index += MAXHAND;
		}

		un = t;
		curTurn = 0;
		prevTurn = 3;

		curComb = Combination::None;
		curFlNum = -1;
		curValue = 0;
		passCnt = 0;
		userCnt = USERNUM;
		gameStart = false;
		tichuCall = 0;

		submitCheck = false;
		lastSubmit = -1;
		Dra = -1;
		Maj = -1;

		for (int i = 0; i < USERNUM; i++){
			score[i] = 0;
			passCall[i] = false;
		}

	}

};