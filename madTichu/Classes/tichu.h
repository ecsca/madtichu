#include <string.h>
#include <algorithm>
#include <time.h>


#define MAXHAND 14
#define MAXCARD 56
#define USERNUM 4
enum Suit { Diamonds = 1, Hearts, Clubs, Spades, Special };
enum Combination { None = 0, Single, Pair, ConPair, Triple, FullHouse, Straight, Bomb, Bird, Dog };