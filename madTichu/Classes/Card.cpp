#include "Card.h"
#include <sstream>

Suit Card::getSuit(){
	return suit;
}
int Card::getValue(){
	return value;
}
void Card::setSubmit(bool _submit){
	submit = _submit;
}
bool Card::getSubmit(){
	return submit;
}
bool Card::canUse(){
	return use;
}
void Card::setCanUse(bool _canUse){
	use = _canUse;
}

bool Card::getFloor(){
	return isFloor;
};
void Card::setFloor(bool _isFloor){
	isFloor = _isFloor;
};

std::string Card::toString(){
	std::string Result;          // string which will contain the result
	std::ostringstream convert;   // stream used for the conversion
	convert << (value + 1);      // insert the textual representation of 'Number' in the characters in the stream
	Result = convert.str();
	if (suit == Suit::Clubs){
		return "Clubs_" + Result;
	}
	else if (suit == Suit::Diamonds){
		return "Diamonds_" + Result;
	}
	else if (suit == Suit::Hearts){
		return "Hearts_" + Result;
	}
	else if (suit == Suit::Spades){
		return "Spades_" + Result;
	}
	else{
		if (value == -10)
			return "MahJong";
		else if (value == -20)
			return "Phoenix";
		else if (value == -30)
			return "DasHund";
		else if (value == -40)
			return "Drache";
	}
}
