#include "GameHelper.h"
#include <sstream>
bool GameHelper::isBomb(Suit s[], int v[], int cnt){
	int i;
	if (isStraight(s, v, cnt)){
		for (i = 1; i < cnt; i++){
			if (s[i - 1] != s[i]) return false;
		}
	}
	else{
		for (i = 1; i < 4; i++){
			if (v[i - 1] != v[i]) return false;
		}
	}
	return true;
}
bool GameHelper::isConPair(Suit s[], int v[], int cnt){
	int i, chance = 0;
	int same = 0;

	if (cnt % 2 != 0) return false;
	if (v[0] == -20){
		chance = 1;
	}
	for (i = 1 + chance; i < cnt; i++){
		if (v[i] == v[i - 1]){
			if (same == 0) same++;
			else return false;
		}
		else if (v[i] == v[i - 1] + 1){
			if (same == 0){
				if (chance == 0) return false;
				else chance--;
			}
			else if (same == 1){
				same = 0;
				continue;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}
	if (same == 0 && chance == 0) return false;
	v[0] = v[1];
	return true;
}
bool GameHelper::isStraight(Suit s[], int v[], int cnt){
	int i, chance = 0;
	if (v[0] == -20){
		chance = 1;
	}
	for (i = 1 + chance; i < cnt; i++){
		if (v[i] != v[i - 1] + 1){
			if (chance == 0) return false;
			if (chance == 1 && v[i] != v[i - 1] + 2) return false;
			chance--;
		}
	}

	return true;
}
bool GameHelper::isFullHouse(Suit s[], int v[], int cnt){
	if (cnt != 5) return false;
	if (v[0] == -20){
		if (v[1] == v[2] && v[3] == v[4]){
			v[0] = v[3];
			return true;
		}
	}
	else{
		if (v[0] == v[1] && v[3] == v[4]){
			if (v[1] == v[2]){
				return true;
			}
			else if (v[2] == v[3]){
				v[0] = v[4];
				return true;
			}
		}
	}

	return false;
}
int GameHelper::callCard(){
	return 1;
}

bool GameHelper::isAvailable(Card** hand){
	int cnt = 0;
	int i;
	Suit s[MAXHAND];
	int v[MAXHAND];
	Combination _curComb = None;
	double _curValue = 0;

	for (i = 0; i < MAXHAND; i++){
		if (hand[i]->getSubmit()){
			s[cnt] = hand[i]->getSuit();
			v[cnt] = hand[i]->getValue();
			cnt++;
		}
	}
	std::sort(s, s + cnt);
	std::sort(v, v + cnt);

	
	if (cnt == 0) return false;
	//printf("========%f %d===\n", _curValue, _curComb);
	switch (cnt){
	case 1:
	{
			  _curComb = Single;
			  _curValue = v[0];
			  if (v[0] == -20){
				  _curValue = curValue + 0.5;
			  }
			  if (v[0] == -40) _curValue = 20;
			  if (v[0] == -10){
				 // _curValue = callCard();
				  _curComb = Combination::Bird;
				  _curValue = -10;
			  }
			  if (v[0] == -30){
				  /* �� ó�� */
				  _curComb = Combination::Dog;
				  _curValue = 0;
				//  _curComb = Combination::Bird;
			  }
			  break;
	}
	case 2:
	{
			  /*��Ȳó��*/
			  if (v[0] == -20){
				  v[0] = v[1];
			  }

			  if (v[0] != v[1]) return false;
			  _curComb = Pair;
			  _curValue = v[0];
			  break;
	}
	case 3:
	{
			  /*��Ȳó��*/
			  if (v[0] == -20){
				  for (i = 2; i < 3; i++){
					  if (v[i - 1] != v[i]) return false;
				  }
				  v[0] = v[1];
			  }

			  for (i = 1; i < 3; i++){
				  if (v[i - 1] != v[i]) return false;
			  }
			  _curComb = Triple;
			  _curValue = v[0];
			  break;
	}
	case 4:
	{
			  if (isConPair(s, v, cnt)){
				  _curComb = ConPair;
				  _curValue = v[0];
				  break;
			  }
			  if (isBomb(s, v, cnt)){
				  _curComb = Bomb;
				  _curValue = v[0];
				  break;
			  }
			  return false;
	}

	default:
	{
			   if (isFullHouse(s, v, cnt)){
				   _curComb = FullHouse;
				   _curValue = v[0];
				   break;
			   }
			   if (isBomb(s, v, cnt)){
				   _curComb = Bomb;
				   _curValue = v[0];
				   break;
			   }
			   if (isConPair(s, v, cnt)){
				   _curComb = ConPair;
				   _curValue = v[0];
				   break;
			   }
			   if (isStraight(s, v, cnt)){

				   _curComb = Straight;
				   if (v[0] == -20){
					   _curValue = v[1];
				   }
				   else _curValue = v[0];

				   break;
			   }
			   return false;
	}
	}

	if ((curFlNum != -1) && (_curValue <= curValue || _curComb != curComb)){
		if (_curComb == Combination::Bomb){
			if (_curComb == curComb && _curValue <= curValue) return false;
			curFlNum = cnt;
			curValue = _curValue;
			curComb = _curComb;
			return true;
		}
		else if (Maj != -1){
			if (_curComb != Combination::Single) return false;
			if (_curValue == Maj){
				curFlNum = cnt;
				curValue = _curValue;
				curComb = _curComb;
				Maj = -1;
				return true;
			}
			for (i = 0; i < 14; i++){
				if (hand[i]->getValue() == Maj) return false;
			}
			if (curComb == Combination::Bird){
				curFlNum = cnt;
				curValue = _curValue;
				curComb = _curComb;
				return true;
			}
			if (_curComb != curComb || _curValue <= curValue) return false;
			curFlNum = cnt;
			curValue = _curValue;
			curComb = _curComb;
			return true;
		}
		else{
			return false;
		}
	}


	if (curFlNum == -1){
		if (_curComb != Combination::Dog) curFlNum = cnt;
	}
	curValue = _curValue;
	curComb = _curComb;

	/*
	for (i = 0; i < MAXHAND; i++){
		if (hand[i]->getSubmit()){
			hand[i]->setSubmit(false);
			hand[i]->setCanUse(false);
		}
	}*/

	return true;
}
int GameHelper::nextTurn(){
	int res = curTurn;
	//prevTurn = curTurn;
	int cnt = 0;

	while (true){
		res = (res + 1) % USERNUM;
		if (users[res]->getEnd() && submitCheck) return res;
		if (!users[res]->getEnd() && !passCall[res]) return res;
		cnt++;
	}
}


bool GameHelper::canSubmit(int userNum){
	if (curTurn != userNum){
		//printf("*********\n");
		return false;
	}
	if (!isAvailable(users[(4 + userNum - un) % 4]->getHand())){
		//printf("^^^^^^^^^\n");
		return false;
	}
}

bool GameHelper::submit(int userNum){
	if (!gameStart) return false;
	if (canSubmit(userNum)){
		lastSubmit = userNum;
		//memcpy(floor[floorCnt++], users[userNum]->getHand(), curFlNum * 4);
		users[userNum]->setNumAvailCard(users[userNum]->getNumAvailCard() - curFlNum);
	}
	else{
		return false;
	}
	
	if (!users[userNum]->getEnd() && users[userNum]->getNumAvailCard() == 0){
		users[userNum]->gameEnd(USERNUM - userCnt + 1);
		submitCheck = true;
	//	printf("===rank : %d  %d\n", users[curTurn]->getRank(), users[curTurn]->getScoreCnt());
	}
	prevPass = false;
//	curTurn = nextTurn();

	return true;
}

bool GameHelper::pass(){
	int i, j;
	//return false;

	if (!gameStart) return false;

	/*
	if (prevPass) passCnt++;
	else passCnt = 1;

	prevPass = true;*/
	//int curTurn = nextTurn();
	
	passCnt++;
	passCall[curTurn] = true;
	if (passCnt == userCnt - 1){
		if (submitCheck){
			userCnt--;
			submitCheck = false;
		}
		if (lastSubmit != -1){
			int sum = 0;
			for (i = 0; i < 56; i++){
				if (_card[i]->getFloor()){
					if (_card[i]->getValue() == 4){
						sum += 5;
					}
					else if (_card[i]->getValue() == 9 || _card[i]->getValue() == 12){
						sum += 10;
					}
					else if (_card[i]->getValue() == -20){
						sum -= 25;
					}
					else if (_card[i]->getValue() == -40){
						sum += 25;
					}
					_card[i]->setFloor(false);
				}
			}
			if (Dra == -1){
				score[lastSubmit] += sum;
			}
			else{
				score[Dra] += sum;
				Dra = -1;
			}
		}
		prevTurn = curTurn;
		curTurn = lastSubmit;

		Maj = -1;
		lastSubmit = -1;
		curComb = None;
		curFlNum = -1;
		curValue = 0;
		passCnt = 0;
		floorCnt = 0;
		for (i = 0; i < 4; i++){
			passCall[i] = false;
		}
		return true;
	}
	return false;
}

void GameHelper::calculateScore(){
}

std::string GameHelper::toString(){
	std::string Result;          // string which will contain the result
	std::ostringstream convert, convert2;   // stream used for the conversion

	std::string s = "";
	convert2 << curTurn;
	Result = convert2.str();
	s += "turn : " + Result;
	s += " comb : ";
	if (curComb == 1){
		s += "Single";
	}
	else if (curComb == 2){
		s += "Pair";
	}
	else if (curComb == 3){
		s += "ConPair";
	}
	else if (curComb == 4){
		s += "Triple";
	}
	else if (curComb == 5){
		s += "FullHouse";
	}
	else if (curComb == 6){
		s += "Straight";
	}
	else if (curComb == 7){
		s += "Bomb";
	}
	else if (curComb == 8){
		s += "Bird";
	}
	else if (curComb == 9){
		s += "Dog";
	}

	
	convert << (int)curValue;      // insert the textual representation of 'Number' in the characters in the stream
	Result = convert.str();
	s += " value : " + Result;
/*	int curTurn; // ���� user
	Combination curComb; // ���� �ٴڿ� �� ����
	int curFlNum; // ���� �ٴڿ� �� ī�� ��
	double curValue; // �ٴڿ� �� ī�� ��*/
	return s;
}

std::string GameHelper::toString_score(){
	std::string Result;          // string which will contain the result
	std::ostringstream convert, convert2, convert3, convert4;   // stream used for the conversion
	convert << (int)score[0];      // insert the textual representation of 'Number' in the characters in the stream
	convert2 << (int)score[1];
	convert3 << (int)score[2];
	convert4 << (int)score[3];
	
	Result = convert.str();

	std::string s = "";
	s += "user0 : " + Result;
	Result = convert2.str();
	s += " user1 : " + Result;
	Result = convert3.str();
	s += " user2 : " + Result;
	Result = convert4.str();
	s += " user3 : " + Result;
	return s;
}
int GameHelper::getCurTurn(){
	return curTurn;
}