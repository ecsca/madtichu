#ifndef __TABLE_H__
#define __TABLE_H__

#include "cocos2d.h"
#include "GameHelper.h"

class Table : public cocos2d::Layer
{
private:
	Card* _card[MAXCARD];
	GameHelper g;
	cocos2d::CCClippingNode* clip;

	cocos2d::Sprite * card[56];
	cocos2d::Sprite * current_card;
	cocos2d::Sprite * current_floor[56];
	int cfloor;

	cocos2d::Sprite * submit;
	cocos2d::Sprite * pass;

	cocos2d::LabelTTF* pLabel;
	cocos2d::LabelTTF* scoreBoard;
	cocos2d::Sprite* hole;
	int depth;

	float fixX, fixY;
	float holeX, holeY;

public :
	
	static cocos2d::Scene* createTable();

	virtual bool init();

	CREATE_FUNC(Table);

	cocos2d::CCSize winSize;
	void initBg();
	void initGame();

	void addCard(int x, int y, int z, int num, Card* _card);
	void initListener();
	virtual bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
	//void onTouchesBegan(CCSet* pTouches, CCEvent *pEvent);

	virtual void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
	virtual void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
	//virtual void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* event);

	//void Table::spriteMoveFinished(CCNode *node);
	//void ccTouchBegan(CCSet *pTouches, CCEvent *pEvent);

};

#endif