
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "network/HttpClient.h"
using namespace cocos2d::network;

class Login : public cocos2d::LayerColor, public cocos2d::extension::EditBoxDelegate
{
private:
	cocos2d::extension::EditBox* m_pEditName;
	cocos2d::extension::EditBox* m_pEditPassword;
	std::string rId;
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	CREATE_FUNC(Login);

	void initEditBox();

	void doPushScene();
	/*
	void doPushSceneTran(cocos2d::Object* pSender);
	void doReplaceScene(cocos2d::Object* pSender);
	void doReplaceSceneTran(cocos2d::Object* pSender);
	*/
	void getLogin(cocos2d::Object* pSender);
	void checkLogin(HttpClient *sender, HttpResponse *response);
	cocos2d::TransitionScene* createTransition(int nIndex, float t, cocos2d::Scene* s);
	virtual void editBoxEditingDidBegin(cocos2d::extension::EditBox* editBox);
	virtual void editBoxEditingDidEnd(cocos2d::extension::EditBox* editBox);
	virtual void editBoxTextChanged(cocos2d::extension::EditBox* editBox, const std::string& text);
	virtual void editBoxReturn(cocos2d::extension::EditBox* editBox);


	//cocos2d::LabelTTF* _labelStatusCode;
};
