#include "Card.h"

class User{
private:
	Card* hand[MAXHAND]; // 들고있는 손패
	Card* score[MAXCARD]; // 점수로 얻은 카드
	int scoreCnt;

	bool end;
	int rank;
	

public:
	//미완성
	int numAvailCard;
	Card** getHand();
	Card** getScore();
	void setScore(Card* _score);
	bool getEnd();
	int getRank();
	void gameEnd(int _rank);
	int getNumAvailCard();
	void setNumAvailCard(int a);
	int getScoreCnt();

	User(Card** _hand){
		memcpy(hand, _hand, MAXHAND*sizeof(Card *));
		end = false;
		rank = 4;
		numAvailCard = MAXHAND;
		scoreCnt = 0;
	}
};
