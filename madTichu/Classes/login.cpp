#include "login.h"
#include "Menus.h"
USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::extension;

Scene* Login::createScene()
{
	auto scene = Scene::create();

	auto layer = Login::create();

	scene->addChild(layer);

	return scene;
}

bool Login::init()
{
	if (!LayerColor::initWithColor(Color4B(255, 255, 255, 255)))
	{
		return false;
	}

	/////////////////////////////

	// 메뉴 아이템 생성 및 초기화

	auto item1 = MenuItemFont::create(
		"Login",
		CC_CALLBACK_1(Login::getLogin, this));
	item1->setColor(Color3B(0, 0, 0));

	// 메뉴 생성
	auto pMenu = Menu::create(item1, NULL);

	// 메뉴 정렬z
	pMenu->alignItemsVertically();

	// 레이어에 메뉴 객체 추가
	this->addChild(pMenu);
	initEditBox();

	//_labelStatusCode = LabelTTF::create("HttpStatusCode", "Courier New", 20);
	//_labelStatusCode->setPosition(Point(240, 260));
	//_labelStatusCode->setColor(Color3B::BLUE);
	//addChild(_labelStatusCode);
	return true;
}

void Login::getLogin(Object* pSender)
{
	std::string cid = m_pEditName->getText();
	std::string passwd = m_pEditPassword->getText();
	HttpRequest*  req = new HttpRequest();
	std::string url = "http://bit.sparcs.org:12312/login/";
	url = url + cid;
	url = url + "/";
	url = url + passwd;
	req->setUrl(url.c_str());
	req->setRequestType(HttpRequest::Type::GET);
	req->setResponseCallback(this, httpresponse_selector(Login::checkLogin));
	HttpClient::getInstance()->send(req);
	req->release();

}

void Login::checkLogin(HttpClient *sender, HttpResponse *response)
{
	std::vector<char> *buffer = response->getResponseData();
	std::string strRes = std::string(buffer->begin(), buffer->end());
	if (strRes.length() > 100)
		return;
	std::string parsedRes = "";
	char *rest = NULL;
	char res[2000];
	//_labelStatusCode->setString(strRes);
	strcpy(res, strRes.c_str());
	parsedRes = strtok(res, ",");
	if (!strcmp(parsedRes.c_str(), "Wrong ID or password"))
	{
		//_labelStatusCode->setString(std::to_string(parsedRes.length()));
	}
	else
	{
		//_labelStatusCode->setString(parsedRes);
		rId = parsedRes;
		doPushScene();
	}
}
void Login::doPushScene()
{
	// 두번째 장면
	auto pScene = Menus::createScene(this->rId);
	Director::getInstance()->pushScene(pScene);
}

void Login::initEditBox(){
	Size editBoxSize = Size(300, 60);

	// top
	m_pEditName = EditBox::create(editBoxSize,
	Scale9Sprite::create("orange_edit.png"));
	m_pEditName->setPosition(Point(240, 250));
	m_pEditName->setFontColor(Color3B::GREEN);
	m_pEditName->setPlaceHolder("Name:");
	m_pEditName->setMaxLength(8);
	m_pEditName->setReturnType(EditBox::KeyboardReturnType::DONE);
	m_pEditName->setDelegate(this);
	this->addChild(m_pEditName);

	// middle
	m_pEditPassword = EditBox::create(editBoxSize,
	Scale9Sprite::create("green_edit.png"));
	m_pEditPassword->setPosition(Point(240, 150));
	m_pEditPassword->setFontColor(Color3B::RED);
	m_pEditPassword->setPlaceHolder("Password:");
	m_pEditPassword->setMaxLength(6);
	m_pEditPassword->setInputFlag(EditBox::InputFlag::PASSWORD);
	m_pEditPassword->setInputMode(EditBox::InputMode::SINGLE_LINE);
	m_pEditPassword->setDelegate(this);
	this->addChild(m_pEditPassword);
}




/*
void Login::doPushSceneTran(Object* pSender)
{
	static int nIndex1 = 0;

	// 두번째 장면
	auto pScene = Menus::createScene();
	Director::getInstance()->pushScene(createTransition(nIndex1, 1, pScene));

	nIndex1++;
	if (nIndex1 > 40)
	{
		nIndex1 = 0;
	}
}

void Login::doReplaceScene(Object* pSender)
{
	// 세번째 장면
	auto pScene = Menus::createScene();
	Director::getInstance()->replaceScene(pScene);
}

void Login::doReplaceSceneTran(Object* pSender)
{
	static int nIndex2 = 40;

	// 세번째 장면
	auto pScene = Menus::createScene();
	Director::getInstance()->replaceScene(createTransition(nIndex2, 1, pScene));

	nIndex2--;
	if (nIndex2 < 0)
	{
		nIndex2 = 40;
	}
}

*/

void Login::editBoxEditingDidBegin(EditBox* editBox)
{
	log("editBox %p DidBegin !", editBox);
}

void Login::editBoxEditingDidEnd(EditBox* editBox)
{
	log("editBox %p DidEnd !", editBox);
}

void Login::editBoxTextChanged(EditBox* editBox, const std::string& text)
{
	log("editBox %p TextChanged, text: %s ", editBox, text.c_str());
}

void Login::editBoxReturn(EditBox* editBox)
{
	log("editBox %p was returned !");
}

TransitionScene* Login::createTransition(int nIndex, float t, Scene* s)
{
	Director::getInstance()->setDepthTest(false);

	switch (nIndex)
	{
		// 점프하면서 Zoom
	case 0: return TransitionJumpZoom::create(t, s);


		// 시계방향으로 침이 돌면서 장면이 바뀜
	case 1: return TransitionProgressRadialCCW::create(t, s);
		// 시계반대방향으로 침이 돌면서 장면이 바뀜
	case 2: return TransitionProgressRadialCW::create(t, s);
	case 3: return TransitionProgressHorizontal::create(t, s);
	case 4: return TransitionProgressVertical::create(t, s);
	case 5: return TransitionProgressInOut::create(t, s);
	case 6: return TransitionProgressOutIn::create(t, s);

		// 교차
	case 7: return TransitionCrossFade::create(t, s);


		// 페이지넘김형식 : PageTransitionForward
	case 8: return TransitionPageTurn::create(t, s, false);
		// 페이지넘김형식 : PageTransitionBackward
	case 9: return TransitionPageTurn::create(t, s, true);
		// 바둑판무늬 좌측하단부터 우측상단순으로 사라짐
	case 10: return TransitionFadeTR::create(t, s);
		// 바툭판무늬 우측상단부터 좌측하단순으로 사라짐
	case 11: return TransitionFadeBL::create(t, s);
		// 하단에서 상단으로 잘라냄
	case 12: return TransitionFadeUp::create(t, s);
		// 상단에서 하단으로 잘라냄
	case 13: return TransitionFadeDown::create(t, s);


		// 바둑판무늬 뿌리기
	case 14: return TransitionTurnOffTiles::create(t, s);


		// 가로로 세등분 나눔
	case 15: return TransitionSplitRows::create(t, s);
		// 세로로 세등분 나눔, 양끝의 두둥분은 밑으로 나머지는 위로
	case 16: return TransitionSplitCols::create(t, s);


		// 페이드인아웃 : 검정 화면
	case 17: return TransitionFade::create(t, s);
		// 페이드인아웃 : 하얀 화면
	case 18: return TransitionFade::create(t, s, Color3B::WHITE);


		// X축(횡선)을 기준으로 회전 : FlipXLeftOver
	case 19: return TransitionFlipX::create(t, s, TransitionScene::Orientation::LEFT_OVER);
		// X축(횡선)을 기준으로 회전 : FlipXRightOver
	case 20: return TransitionFlipX::create(t, s, TransitionScene::Orientation::RIGHT_OVER);
		// Y축(종선)을 기준으로 회전 : FlipYUpOver
	case 21: return TransitionFlipY::create(t, s, TransitionScene::Orientation::UP_OVER);
		// Y축(종선)을 기준으로 회전 : FlipYDownOver
	case 22: return TransitionFlipY::create(t, s, TransitionScene::Orientation::DOWN_OVER);
		// 뒤집어지면서 다음장면으로 넘어감 : FlipAngularLeftOver
	case 23: return TransitionFlipAngular::create(t, s, TransitionScene::Orientation::LEFT_OVER);
		// 뒤집어지면서 다음장면으로 넘어감 : FlipAngularRightOver
	case 24: return TransitionFlipAngular::create(t, s, TransitionScene::Orientation::RIGHT_OVER);


		// X축(횡선)을 기준으로 회전 (확대) : ZoomFlipXLeftOver
	case 25: return TransitionZoomFlipX::create(t, s, TransitionScene::Orientation::LEFT_OVER);
		// X축(횡선)을 기준으로 회전 (확대) : ZoomFlipXRightOver
	case 26: return TransitionZoomFlipX::create(t, s, TransitionScene::Orientation::RIGHT_OVER);
		// Y축(종선)을 기준으로 회전 (확대) : ZoomFlipYUpOver
	case 27: return TransitionZoomFlipY::create(t, s, TransitionScene::Orientation::UP_OVER);
		// Y축(종선)을 기준으로 회전 (확대) : ZoomFlipYDownOver
	case 28: return TransitionZoomFlipY::create(t, s, TransitionScene::Orientation::DOWN_OVER);
		// 뒤집어지면서 다음장면으로 넘어감 (확대) : ZoomFlipAngularLeftOver
	case 29: return TransitionZoomFlipAngular::create(t, s, TransitionScene::Orientation::LEFT_OVER);
		// 뒤집어지면서 다음장면으로 넘어감 (확대) : ZoomFlipAngularRightOver
	case 30: return TransitionZoomFlipAngular::create(t, s, TransitionScene::Orientation::RIGHT_OVER);


		// 이전장면 수축 다음장면 확대
	case 31: return TransitionShrinkGrow::create(t, s);
		// 회전하면서 Zoom
	case 32: return TransitionRotoZoom::create(t, s);


		// 왼쪽에서 다음장면이 나타나서 이전장면을 덮어씀
	case 33: return TransitionMoveInL::create(t, s);
		// 오른쪽에서 다음장면이 나타남
	case 34: return TransitionMoveInR::create(t, s);
		// 위쪽에서 다음장면이 나타남
	case 35: return TransitionMoveInT::create(t, s);
		// 아래쪽에서 다음장면이 나타남
	case 36: return TransitionMoveInB::create(t, s);


		// 왼쪽에서 다음장면이 나타나서 이전장면을 밀어냄
	case 37: return TransitionSlideInL::create(t, s);
		// 오른쪽에서 다음장면이 나타나서 이전장면을 밀어냄
	case 38: return TransitionSlideInR::create(t, s);
		// 위쪽에서 다음장면이 나타나서 이전장면을 밀어냄
	case 39: return TransitionSlideInT::create(t, s);
		// 아래쪽에서 다음장면이 나타나서 이전장면을 밀어냄
	case 40: return TransitionSlideInB::create(t, s);

	default: break;
	}

	return NULL;
}
