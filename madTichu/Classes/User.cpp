#include "User.h"
Card** User::getHand(){
	return hand;
}
Card** User::getScore(){
	return score;
}
bool User::getEnd(){
	return end;
}
int User::getRank(){
	return rank;
}
void User::gameEnd(int _rank){
	rank = _rank;
	end = true;
}
int User::getNumAvailCard(){
	return numAvailCard;
}
void User::setNumAvailCard(int a){
	numAvailCard = a;
}
int User::getScoreCnt(){
	return scoreCnt;
}
void User::setScore(Card* _score){
	score[scoreCnt++] = _score;
}