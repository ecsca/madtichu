#include "tichu.h"
#include <string>

class Card{
private:
	
	

public:
	Suit suit; // 문양
	int value; // 값
	bool submit; // 제출여부
	bool use; // 사용가능여부
	bool isFloor;


	int clickCnt;

	Suit getSuit();
	int getValue();
	void setSubmit(bool _submit);
	bool getSubmit();
	bool canUse();

	bool getFloor();
	void setFloor(bool _isFloor);

	void setCanUse(bool _canUse);
	std::string toString();

	std::string cardToServer();
	void setCard(char a, char b, char c);


	Card(Suit _suit, int _value){
		suit = _suit;
		value = _value;
		submit = false;
		use = true;
		isFloor = false;
		clickCnt = 0;
	}
	Card(char a,  char b, char c){
		switch (a - '0'){
		case 1:
			suit = Suit::Diamonds;
			break;
		case 2:
			suit = Suit::Hearts;
			break;
		case 3:
			suit = Suit::Clubs;
			break;
		case 4:
			suit = Suit::Spades;
			break;
		default:
			suit = Suit::Special;
		}
		int _value = 0;
		if (suit == Suit::Special){
			if (c - '0' == 1){
				_value = -10;
			}
			else if (c - '0' == 2){
				_value = -20;
			}
			else if (c - '0' == 3){
				_value = -30;
			}
			else if (c - '0' == 4){
				_value = -40;
			}
		}
		else{
			if (b - '0' == 1){
				_value += 10;
			}
			_value += c-'0';
		}
		value = _value;
		submit = false;
		use = true;
		isFloor = false;
		clickCnt = 0;
	}
};