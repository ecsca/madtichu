
#include "cocos2d.h"
#include "GameHelper.h"
#include "network/HttpClient.h"

using namespace cocos2d::network;

class Room : public cocos2d::Layer
{
private:


	Card* _card[MAXCARD];
	GameHelper g;
	cocos2d::CCClippingNode* clip;

	cocos2d::Sprite * card[56];
	cocos2d::Sprite * passCall[4];
	cocos2d::Sprite * current_card;

	float curCardX, curCardY;
	int curDepth;

	float exCardX[3], exCardY[3];
	int exDepth[3];
	bool exLock[3];

	int tichu[4];

	cocos2d::Sprite * current_floor[2][56];
	//Card * curCard[56];

	int cfloor;

	cocos2d::Sprite * submit;
	cocos2d::Sprite * pass;

	cocos2d::Sprite * boader[3];

	cocos2d::Sprite * scBoard;
	cocos2d::LabelTTF* writeScore[4];

	cocos2d::LabelTTF* pLabel;
	cocos2d::LabelTTF* scoreBoard;
	cocos2d::Sprite* sBoard;
	cocos2d::Sprite* hole;

	cocos2d::LabelTTF* borderFirstNum;
	cocos2d::LabelTTF* borderSecondNum;
	cocos2d::LabelTTF* borderThirdNum;

	cocos2d::LabelTTF* num0;
	cocos2d::LabelTTF* num1;
	cocos2d::LabelTTF* num2;
	cocos2d::LabelTTF* num3;
//	cocos2d::Sprite* passCall;
	int depth;

	float fixX, fixY;
	float holeX, holeY;
	float cardX, cardY;

	cocos2d::Menu * dMenu;
	cocos2d::Menu * mMenu;
	cocos2d::Sprite * dBoard;
	cocos2d::Sprite * mBoard;

	int Dra;
	int chamSae;
	int Maj;

public:
	bool buttonClick;
	int t;
	std::string r;

	bool endGame;
	
	int score[4];

	static cocos2d::Scene* createTable(int userNum, std::string roomNum, int s[4]);

	virtual bool init();


	CREATE_FUNC(Room);
	static Room * create(int userNum, std::string roomNum, int s[4]){
		Room * a = new Room();
		if (a != NULL){
			a->t = userNum;
			a->r = roomNum;
			for (int i = 0; i < 4; i++){
				a->score[i] = s[i];
			}
		}
		if (a && a->init()){
			a->autorelease();
			return a;
		}
		else{
			delete a;
			a = NULL;
			return NULL;
		}
	}

	cocos2d::CCSize winSize;
	void initBg();
	void initGame();
	void initClip();
	void initButtons();
	void initValue();
	void initBoard();
	void initBorderNum();
	void eraseBorderNum();
	void setNum();
	void setTurn(int turn);

	void initsBoard();
	void showScore();
	bool sBoardClick;

	void goToHole(cocos2d::Sprite * card, cocos2d::CallFunc * a);
	void showFloor(cocos2d::Sprite * card, float x, float y, float z);
	std::string intToString(int value);

	void largeTichu();
	void largeTichuCallBack(HttpClient *sender, HttpResponse *response);
	void largeTichu2(int call);
	void largeTichuCallBack2(HttpClient *sender, HttpResponse *response);

	void smallTichu();
	void smallTichuCallBack(HttpClient *sender, HttpResponse *response);
	void smallTichu2(int call);
	void smallTichuCallBack2(HttpClient *sender, HttpResponse *response);


	void exchangeCard();
	void exchangeCardCallBack(HttpClient *sender, HttpResponse *response);
	void actionLock(int i);
	//void goToHole2(int s);

	void floor();
	void floorCallBack(HttpClient *sender, HttpResponse *response);
	void submitCard(std::string cards, int f);

	void addCard_user(int x, int y, int z, int num, Card* _card);
	void addCard_other(int x, int y, int z, int num, Card* _card);
	void initListener();
	virtual bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
	//void onTouchesBegan(CCSet* pTouches, CCEvent *pEvent);

	virtual void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
	virtual void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
	//virtual void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* event);

	//void Table::spriteMoveFinished(CCNode *node);
	//void ccTouchBegan(CCSet *pTouches, CCEvent *pEvent);
	void cleanFloor();
	void passCallsetVisibleFalse();
	void game_end();
	void caculate_score();
	void restart();
	void restartCallBack(HttpClient *sender, HttpResponse *response);

	void submitCardD(cocos2d::Object* pSender, int f, int D);
	void selectD(int f);
	void getD();
	void getDCallBack(HttpClient *sender, HttpResponse *response);

	void selectM(int f);
	void submitM(cocos2d::Object* pSender, int f, int M);
	void getM();
	void getMCallBack(HttpClient *sender, HttpResponse *response);

	void yesChamsae();
	void notChamsae();
	void writeMahjong(HttpClient *sender, HttpResponse *response);
};
