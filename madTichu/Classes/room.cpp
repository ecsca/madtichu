#include "Room.h"


USING_NS_CC;

Scene* Room::createTable(int userNum, std::string roomNum, int s[4])
{
	auto scene = Scene::create();
	auto layer = Room::create(userNum, roomNum, s);
	scene->addChild(layer, 0);
	//layer->t = userNum;
	//layer->r = roomNum;
	return scene;
}


bool Room::init()
{
	winSize = CCDirector::sharedDirector()->getWinSize();
	if (!Layer::init())
	{
		return false;
	}
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Point origin = Director::getInstance()->getVisibleOrigin();

	initValue();

	initBg();
	initClip();
	initButtons();
	initsBoard();
	initGame();
	
	
	initListener();
	return true;
}
void Room::initValue(){
	for (int i = 0; i < 3; i++){
		exCardX[i] = 0;
		exCardY[i] = 0;
		exDepth[i] = -1;
	}

	endGame = false;
	depth = 100;
	current_card = NULL;
	cfloor = 0;
}

void Room::initClip(){
	int w = winSize.width;
	int h = winSize.height;
	
	fixX = (w/30);

	clip = CCClippingNode::create();
	clip->setAnchorPoint(ccp(0.5, 0.5));

	clip->setContentSize(Size(w - 2*fixX, h/2- h/6));
	

	CCDrawNode* shape = CCDrawNode::create();
	CCPoint pts[8];
	pts[0] = ccp(0, 0);
	pts[1] = ccp(clip->getContentSize().width, 0);
	pts[2] = ccp(clip->getContentSize().width, clip->getContentSize().height);
	pts[3] = ccp(w/6, clip->getContentSize().height);
	pts[4] = ccp(w/6, clip->getContentSize().height + h/30);
	pts[5] = ccp(0, clip->getContentSize().height + h / 30);
	pts[6] = ccp(0, clip->getContentSize().height);
	pts[7] = ccp(0, clip->getContentSize().height);
	shape->drawPolygon(pts, 8, ccc4f(1, 1, 1, 1), 0, ccc4f(1, 1, 1, 1));

	clip->setStencil(shape);
	this->addChild(clip, 2);
	fixY = (h / 30);
	clip->setPosition(ccp(winSize.width / 2, clip->getContentSize().height / 2 + fixY));

	CCSprite * bg = CCSprite::create("woodBackground_hand.png");
	bg->setAnchorPoint(ccp(0.5, 0.5));
	bg->setPosition(ccp(clip->getContentSize().width / 2, clip->getContentSize().height / 2));
	clip->addChild(bg, 3);

}
void Room::initBg()
{
	CCSprite * bg = CCSprite::create("woodBackground.jpg");
	bg->setPosition(winSize.width / 2, winSize.height / 2);
	this->addChild(bg, 0);


	for (int i = 0; i < 4; i++)
	{
		passCall[i] = CCSprite::create("passCall.png");
		passCall[i]->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		int origX = passCall[i]->getBoundingBox().size.width;
		int origY = passCall[i]->getBoundingBox().size.height;
		passCall[i]->setScaleX(winSize.width / (4 * origX));
		passCall[i]->setScaleY(winSize.height / (16 * origY));
		passCall[i]->setZOrder(300);
		passCall[i]->setVisible(false); 
	}
	passCall[2]->setPosition(ccp(winSize.width / 2, winSize.height - winSize.height / 6 + winSize.height / 30));
	auto rotate1 = RotateTo::create(0, 180);
	passCall[2]->runAction(CCSequence::create(rotate1, NULL));

	passCall[3]->setPosition(ccp(winSize.width / 2 - winSize.width / 4, winSize.height * 13 / 20 + winSize.height / 30));
	auto rotate2 = RotateTo::create(0, 90);
	passCall[3]->runAction(CCSequence::create(rotate2, NULL));

	passCall[1]->setPosition(ccp(winSize.width / 2 + winSize.width / 4, winSize.height * 13 / 20 + winSize.height / 30));
	auto rotate3 = RotateTo::create(0, -90);
	passCall[1]->runAction(CCSequence::create(rotate3, NULL));
	this->addChild(passCall[0], 100);
	this->addChild(passCall[1], 100);
	this->addChild(passCall[2], 100);
	this->addChild(passCall[3], 100);
}

void Room::initGame()
{
	buttonClick = true;

	float w = winSize.width;
	float h = winSize.height;
	g = GameHelper(t);
	memcpy(_card, g._card, sizeof(_card));

	float originalX = 80;
	float originalY = 100;
	cardX = w / (7.2*originalX);
	cardY = h / (10*originalY);
	largeTichu();

	/*
	for (int i = 0; i < 8; i++)
	{
		addCard_user(w/2, h/2, 50 + i, i, _card[i]);
	}

	for (int i = 14; i < 56; i++)
	{
		addCard_other(w / 2, h / 2, 50 + i, i, _card[i]);
	}

	for (int i = 0; i < 3; i++){
		boader[i] = Sprite::create("boader.png");
		float origX = boader[i]->getBoundingBox().size.width;
		float origY = boader[i]->getBoundingBox().size.height;

		boader[i]->setScaleX(cardX*1.1);
		boader[i]->setScaleY(cardY*1.1);

		float dx, dy;

		if (i == 0){
			dx = clip->getContentSize().width / 2;
			dy = clip->getContentSize().height / 2;
		}
		else if (i == 1){
			dx = clip->getContentSize().width / 2 - boader[i]->getBoundingBox().size.width - winSize.width / 20;
			dy = clip->getContentSize().height / 2;
		}
		else if (i == 2){
			dx = clip->getContentSize().width / 2 + boader[i]->getBoundingBox().size.width + winSize.width / 20;
			dy = clip->getContentSize().height / 2;
		}
		boader[i]->setAnchorPoint(ccp(0.5, 0.5));
		boader[i]->setPosition(ccp(dx, dy));
		clip->addChild(boader[i], 100);
	}
	*/
	
}
void Room::addCard_other(int x, int y, int z, int num, Card* _card){
	float w = winSize.width;
	float h = winSize.height;

	_card->setCard('1', '0', '0'+ -1);
	card[num] = Sprite::create("Back.png");

	card[num]->setAnchorPoint(ccp(0.5, 0.5));
	card[num]->setScaleX(cardX);
	card[num]->setScaleY(cardY);

	this->addChild(card[num], z);

	if (num > 41){
		auto rotate1 = RotateTo::create(0, 90);

		float gap = h / 2 * 2 / 3;
		float cardWidth = card[num]->getBoundingBox().size.width;
		float dx = 0;
		float dy = gap / 14 * (num - 41) + h / 2 + h / 20;
		card[num]->setPosition(ccp(dx, dy));
		card[num]->runAction(CCSequence::create(rotate1, NULL));
		
	}
	else if (num > 27){
		float gap = w/2;
		float cardWidth = card[num]->getBoundingBox().size.width;
		float dx = gap / 14 * (num - 27) + gap/2;
		float dy = h;

		card[num]->setPosition(ccp(dx, dy));
	}
	else if (num > 13){
		auto rotate1 = RotateTo::create(0, -90);

		float gap = h / 2 * 2 / 3;
		float cardWidth = card[num]->getBoundingBox().size.width;
		float dx = w;
		float dy = gap / 14 * (num - 13) + h / 2 +h / 20;
		card[num]->setPosition(ccp(dx, dy));
		card[num]->runAction(CCSequence::create(rotate1, NULL));
	}
	
}

void Room::addCard_user(int x, int y, int z, int num, Card* _card)
{
	
	card[num] = Sprite::create("Back.png");
	card[num]->setPosition(ccp(x, y));
	card[num]->setAnchorPoint(ccp(0.5, 0.5));
	card[num]->setScaleX(cardX);
	card[num]->setScaleY(cardY);

	clip->addChild(card[num], z);
	int actualDuration = 1;

	float gap = clip->getContentSize().width;
	float cardWidth = card[num]->getBoundingBox().size.width;
	float dx = gap / 14 * (num + 1);
	float dy = card[num]->getBoundingBox().size.height / 2;

	card[num]->setTexture(Director::getInstance()->getTextureCache()->addImage(_card->toString() + ".png"));
	CCFiniteTimeAction *deal = CCMoveTo::create(actualDuration, ccp(dx, dy));
	card[num]->runAction(CCSequence::create(deal, NULL));

}
void Room::initButtons(){
	float w = winSize.width;
	float h = winSize.height;

	float dy = clip->getContentSize().height + h / 30;
	float dx = 0;
	
	dx = w / 6 + w/30;
	submit = Sprite::create("submit.png");
	float origX = submit->getBoundingBox().size.width;
	float origY = submit->getBoundingBox().size.height;

	submit->setScaleX(w/ (6 * origX));
	submit->setScaleY(h / (30 * origY));


	submit->setAnchorPoint(ccp(0, 0));
	submit->setPosition(ccp(dx , dy));
	this->addChild(submit, 100);

	dx = dx + w / 6;
	pass = Sprite::create("pass.png");
	origX = pass->getBoundingBox().size.width;
	origY = pass->getBoundingBox().size.height;

	pass->setScaleX(w / (6 * origX));
	pass->setScaleY(h / (30 * origY));

	pass->setAnchorPoint(ccp(0, 0));
	pass->setPosition(ccp(dx, dy));
	this->addChild(pass, 100);

	dx = dx + w / 6;
	sBoard = Sprite::create("sBoard.png");
	origX = sBoard->getBoundingBox().size.width;
	origY = sBoard->getBoundingBox().size.height;

	sBoard->setScaleX(w / (6 * origX));
	sBoard->setScaleY(h / (30 * origY));

	sBoard->setAnchorPoint(ccp(0, 0));
	sBoard->setPosition(ccp(dx, dy));
	this->addChild(sBoard, 100);



	pLabel = LabelTTF::create("ABC", "Hevetica", 32);
	pLabel->setPosition(Point(w / 2, h / 2 + h/4));
	pLabel->setColor(Color3B(255, 255, 0));
	
	pLabel->setVisible(false);
	this->addChild(pLabel, 100);

	scoreBoard = LabelTTF::create("ABC", "Hevetica", 32);
	scoreBoard->setPosition(Point(w / 2, h / 2 + h / 4 + h/30));
	scoreBoard->setColor(Color3B(255, 255, 0));
	
	this->addChild(scoreBoard, 100);



	/*
	passCall = Sprite::create("passCall.png");
	passCall->setPosition(Point(w / 2, h / 2));
	this->addChild(passCall, 0);
	origX = passCall->getBoundingBox().size.width;
	origY = passCall->getBoundingBox().size.height;
	passCall->setScaleX(w / (4* origX));
	passCall->setScaleY(h / (16* origY));
	passCall->setVisible(false);*/

}
void Room::initListener()
{
	auto listener = EventListenerTouchOneByOne::create();

	listener->onTouchBegan = CC_CALLBACK_2(Room::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(Room::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(Room::onTouchEnded, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void Room::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event)
{
	auto touchPoint = touch->getLocation();
	if (current_card == NULL) return;
	//current_card->setZOrder(100);
	bool bTouch = current_card->getBoundingBox().containsPoint(touchPoint);

	if (bTouch)
	{
		current_card->setPosition(touchPoint.x - fixX, touchPoint.y - fixY);
	}
}

void Room::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event){
	if (g.gameStart || g.tichuCall != 2){
		current_card = NULL;
	}
	else{
		auto touchPoint = touch->getLocation();
		Point realTouch = Point(touchPoint.x - fixX, touchPoint.y - fixY);
		if (current_card == NULL) return;

		for (int i = 0; i < 3; i++){
			if (exDepth[i] == -1){
				if (boader[i]->getBoundingBox().containsPoint(realTouch)){
					exCardX[i] = curCardX; exCardY[i] = curCardY;
					exDepth[i] = curDepth;
					current_card->setPosition(ccp(boader[i]->getPositionX(), boader[i]->getPositionY()));
					current_card->setZOrder(curDepth);
					current_card = NULL;
					return;
					//	current_card->setZOrder(curDepth);
				}
			}
			else{
				if (!boader[i]->getBoundingBox().containsPoint(realTouch) && curDepth == exDepth[i]){
					curCardX = exCardX[i]; curCardY = exCardY[i];
					curDepth = exDepth[i];
					exCardX[i] = 0; exCardY[i] = 0;
					exDepth[i] = -1;
					current_card->setPosition(ccp(curCardX, curCardY));
					current_card->setZOrder(curDepth);
					current_card = NULL;
					return;
				}
				
			}
		}
		
		current_card->setPosition(ccp(curCardX, curCardY));
		current_card->setZOrder(curDepth);
		current_card = NULL;
		return;
		
	}
	
}
bool Room::onTouchBegan(Touch* touch, Event* events)
{
	auto touchPoint = touch->getLocation();
	if (sBoardClick){
		scBoard->setVisible(false);
		for (int i = 0; i < 4; i++){
			writeScore[i]->setVisible(false);
		}
		sBoardClick = false;
		return true;
	}

	if (sBoard->getBoundingBox().containsPoint(touchPoint)){
		if (!sBoardClick){
			showScore();
			sBoardClick = true;
		}
		return true;
	}
	

	if (g.users[t]->numAvailCard == 0){
		if (g.submitCheck){
			if (pass->getBoundingBox().containsPoint(touchPoint)){
				passCall[0]->setVisible(true);
				if (g.pass()){
					cleanFloor();
					submitCard("0", 0);
				}
				else{
					submitCard("0", 0);
				}
			}
		}
		if (endGame){
			if (submit->getBoundingBox().containsPoint(touchPoint)){
				restart();
			}
			return true;
		}
		return false;
	}
	if (g.tichuCall == 0){
		if (!buttonClick) return false;
		if (submit->getBoundingBox().containsPoint(touchPoint)){
			largeTichu2(1);
			buttonClick = false;
		}
		if (pass->getBoundingBox().containsPoint(touchPoint)){
			largeTichu2(0);
			buttonClick = false;
		}
		
		return true;
	}
	else if (g.tichuCall == 1){
		if (buttonClick) return false;
		if (submit->getBoundingBox().containsPoint(touchPoint)){
			if (tichu[0] != 1){
				smallTichu2(2);
				buttonClick = true;
			}
		}
		if (pass->getBoundingBox().containsPoint(touchPoint)){
			smallTichu2(0);
			buttonClick = true;
		}
		
		return true;
	}


	Point realTouch = Point(touchPoint.x - fixX, touchPoint.y - fixY);

	for (int i = 13; i >= 0; i--){
		if (card[i]->getBoundingBox().containsPoint(realTouch)){
			if (!_card[i]->canUse()) return false;
			current_card = card[i];
			curCardX = card[i]->getPositionX();
			curCardY = card[i]->getPositionY();
			curDepth = 50+i;
		//	card[i]->setZOrder(depth++);
			

			if (g.gameStart){
				if (_card[i]->getSubmit()){
					//card[i]->setPosition(ccp(card[i]->getPosition().x + dx[i / 14], card[i]->getPosition().y + dy[i / 14]));
					_card[i]->setSubmit(false);
					card[i]->setColor(Color3B(255, 255, 255));
					return true;
				}
				else{
					//card[i]->setPosition(ccp(card[i]->getPosition().x - dx[i / 14], card[i]->getPosition().y - dy[i / 14]));
					_card[i]->setSubmit(true);
					card[i]->setColor(Color3B(255, 255, 0));
					return true;
				}
			}
			else{
				return true;
			}
		};
	}


	if (submit->getBoundingBox().containsPoint(touchPoint)){
	//	int userNum = g.getCurTurn();
		if (!g.gameStart && buttonClick){
			int cnt = 0;
			for (int i = 0; i < 3; i++){
				if (exDepth[i] != -1) cnt++;
			}
			if (cnt != 3) return false;
			eraseBorderNum();
			for (int i = 0; i < 3; i++){
				exLock[i] = false;
				boader[i]->setVisible(false);
				cocos2d::CallFunc * callback = NULL;
				if (i == 2) callback = CallFunc::create(CC_CALLBACK_0(Room::exchangeCard, this));
				goToHole(card[exDepth[i]-50], callback);
				
			}
		//	exchangeCard();
			buttonClick = false;
			return true;
		}
		else if (g.submit(t)){
			passCall[0]->setVisible(false);

			int s = cfloor;
			std::string res = "";
			for (int i = 13; i >= 0; i--){
				if (_card[i]->getSubmit() && _card[i]->canUse()){
					_card[i]->setFloor(true);
					_card[i]->setSubmit(false);
					_card[i]->setCanUse(false);
					res += _card[i]->cardToServer();
					cocos2d::Sprite* temp = Sprite::create(_card[i]->toString() + ".png");
					temp->setVisible(false);
					this->addChild(temp);
					current_floor[0][cfloor] = temp;
					current_floor[1][cfloor] = card[i];
					current_floor[0][cfloor]->setScaleX(cardX);
					current_floor[0][cfloor++]->setScaleY(cardY);
				}
			}
			float gap;
			
			float x = winSize.width / 2;
			float y = winSize.height / 2;
			if ((cfloor - s) >= 5){
				gap = 3 * card[0]->getBoundingBox().size.width;
			}
			else if ((cfloor - s) >= 3){
				gap = 2 * card[0]->getBoundingBox().size.width;
			}
			else{
				gap = 1 * card[0]->getBoundingBox().size.width;
			}
			for (int i = 1; i <= (cfloor - s); i++){
				int actualDuration = 1;
				float dx, dy;
				dx = winSize.width / 12;
				dy = clip->getContentSize().height - card[i]->getBoundingBox().size.width / 2;

				CCFiniteTimeAction *actionMove = CCMoveTo::create(actualDuration, ccp(dx, dy));
				CCFiniteTimeAction *deal = CCMoveTo::create(actualDuration, ccp(dx, dy + 500));
				
				dx = (gap / (cfloor - s) * i) - (gap / 2) - (gap / (cfloor-s)) / 2;
				//current_floor[0][s + i - 1]->setPosition(ccp(x + dx, y));
				auto callback = CallFunc::create(CC_CALLBACK_0(Room::showFloor, this, current_floor[0][s + i - 1], x+dx, y, i));
				
				current_floor[1][s + i - 1]->runAction(CCSequence::create(actionMove, deal, callback, NULL));
			}
			passCall[0]->setVisible(false);

			if (g.users[t]->numAvailCard == 0){
				if (g.curValue == 20){
					selectD(1);
				}
				else if (g.curValue == -10){
					selectM(1);
				}
				else{
					submitCard(res, 1);
				}
			}
			else{
				if (g.curValue == 20){
					selectD(0);
				}
				else if (g.curValue == -10){
					selectM(0);
				}
				else{
					submitCard(res, 0);
				}
			}
			
			pLabel->setString(g.toString());
		}
		else{
			for (int i = 13; i >=0; i--){
				if (_card[i]->getSubmit() && _card[i]->canUse()){
					_card[i]->setSubmit(false);
					card[i]->setColor(Color3B(255, 255, 255));
					//	card[i]->setPosition(ccp(card[i]->getPosition().x + dx[userNum], card[i]->getPosition().y + dy[userNum]));
				}
			}
			pLabel->setString(g.toString());
		}

	}


	if (g.gameStart && pass->getBoundingBox().containsPoint(touchPoint)){
		if (g.Maj != -1){
			for (int i = 0; i < 14; i++){
				if (_card[i]->getValue() == g.Maj) return false;
			}
		}
		
		if (g.curTurn != t) return false;
		if (g.lastSubmit == -1) return false;
		passCall[0]->setVisible(true);
		if (g.pass()){
			cleanFloor();
			submitCard("0", 0);
			/*
			int actualDuration = 1;
			for (int i = 0; i < MAXCARD; i++){
				if (_card[i]->getFloor()){
					_card[i]->setFloor(false);;

				}
			}
			for (int i = 0; i < cfloor; i++){
				this->removeChild(current_floor[0][i]);
			}*/
		//	scoreBoard->setString(g.toString_score());
		}
		else{
			submitCard("0", 0);
		}
	//	pLabel->setString(g.toString());
	}

	return true;
}
void Room::goToHole(cocos2d::Sprite * card, cocos2d::CallFunc * a){
	int actualDuration = 1;
	float dx, dy;
	dx = winSize.width / 12;
	dy = clip->getContentSize().height - card->getBoundingBox().size.width / 2;

	CCFiniteTimeAction *actionMove = CCMoveTo::create(actualDuration, ccp(dx, dy));
	CCFiniteTimeAction *deal = CCMoveTo::create(actualDuration, ccp(dx, dy + 500));
	if (a == NULL){

		card->runAction(CCSequence::create(actionMove, deal,NULL));
	}
	else{

		card->runAction(CCSequence::create(actionMove, deal, a, NULL));
	}
}
void Room::showFloor(cocos2d::Sprite * card, float x, float y, float z){
	card->setPosition(x, y);
	card->setZOrder(z);
	card->setVisible(true);
}

void Room::actionLock(int i){
	exLock[i] = true;
}
/*
void Room::goToHole2(int s){
	float gap;

	float x = winSize.width / 2;
	float y = winSize.height / 2;
	if (cfloor - s >= 5){
		gap = 5 * card[0]->getBoundingBox().size.width;
	}
	else{
		gap = (cfloor - s) * card[0]->getBoundingBox().size.width;
	}
	for (int i = 1; i <= (cfloor - s); i++){
		float dx = (gap / (cfloor - s) * i) - (gap / 2) - card[0]->getBoundingBox().size.width / 2;
		current_floor[s + i - 1]->setPosition(ccp(x + dx, y));
	}
}*/

void Room::largeTichuCallBack(HttpClient *sender, HttpResponse *response){
	std::vector<char> *buffer = response->getResponseData();
	std::string strRes = std::string(buffer->begin(), buffer->end());
	if (strRes.length() > 1000)
		return;
	std::string parsedRes = "";
	char *rest = NULL;
	char res[2000];
	strcpy(res, strRes.c_str());
	parsedRes = strtok(res, ",");
	std::string temp = parsedRes;

	int j = 0;
	char a[8];
	char b[8];
	char c[8];

	int k = 0;

	for (int i = 0; i < 8; i++){
		a[i] = temp[j];
		b[i] = temp[j + 1];
		if (a[i] == '5') b[i] +=2;
		c[i] = temp[j + 2];
	//	_card[i]->setCard(temp[j], temp[j + 1], temp[j + 2]);//new Card(temp[j], temp[j + 1], temp[j + 2]);
		j += 3;
	}
	for (int i = 0; i < 8; i++){
		for (j = 0; j < 8; j++){
			if ( (b[i] < b[j]) || ( (b[i] == b[j]) && c[i] < c[j])){
				char temp = a[i];
				a[i] = a[j];
				a[j] = temp;

				temp = b[i];
				b[i] = b[j];
				b[j] = temp;

				temp = c[i];
				c[i] = c[j];
				c[j] = temp;
			}
		}
	}

	for (int i = 0; i < 8; i++){
		if (a[i] == '5') b[i] -= 2;
		_card[i]->setCard(a[i], b[i], c[i]);//new Card(temp[j], temp[j + 1], temp[j + 2]);

	}
	for (int i = 0; i < 8; i++)
	{
		addCard_user(winSize.width / 2, winSize.height / 2, 50 + i, i, _card[i]);
	}
	scoreBoard->setString("org : LargeTichu, red : Pass");
}

void Room::largeTichu()
{
	buttonClick = true;

		std::string url = "http://bit.sparcs.org:12312/deal8/";
		url += r;
		url += "/" + intToString(t);
		
		HttpRequest* req = new HttpRequest();
		req->setUrl(url.c_str());
		req->setRequestType(HttpRequest::Type::GET);
		req->setResponseCallback(this, httpresponse_selector(Room::largeTichuCallBack));
		HttpClient::getInstance()->send(req);
		req->release();
}

std::string Room::intToString(int value){
	std::string Result;          // string which will contain the result
	std::ostringstream convert;   // stream used for the conversion
	convert << (value);      // insert the textual representation of 'Number' in the characters in the stream
	Result = convert.str();
	return Result;
}

void Room::largeTichuCallBack2(HttpClient *sender, HttpResponse *response){
	std::vector<char> *buffer = response->getResponseData();
	std::string strRes = std::string(buffer->begin(), buffer->end());
	if (strRes.length() > 1000)
		return;
	std::string parsedRes = "";
	char *rest = NULL;
	char res[2000];
	strcpy(res, strRes.c_str());
	parsedRes = strtok(res, ",");
	std::string temp = parsedRes;
	/*
	int j = 0;
	*/
	
	for (int i = 0; i < 4; i++){
		tichu[i] = 0;
		tichu[i] = temp[i] - '0';
	}
	g.tichuCall = 1;
	smallTichu();
}

void Room::largeTichu2(int call){
	scoreBoard->setString("Wait Other Player");

	std::string url = "http://bit.sparcs.org:12312/Tichu/";
	url += r;
	url += "/" + intToString(t);
	url += "/" + intToString(call);

	HttpRequest* req = new HttpRequest();
	req->setUrl(url.c_str());
	req->setRequestType(HttpRequest::Type::GET);
	req->setResponseCallback(this, httpresponse_selector(Room::largeTichuCallBack2));
	HttpClient::getInstance()->send(req);
	req->release();
}

void Room::smallTichu()
{
	std::string url = "http://bit.sparcs.org:12312/deal6/";
	url += r;
	url += "/" + intToString(t);

	HttpRequest* req = new HttpRequest();
	req->setUrl(url.c_str());
	req->setRequestType(HttpRequest::Type::GET);
	req->setResponseCallback(this, httpresponse_selector(Room::smallTichuCallBack));
	HttpClient::getInstance()->send(req);
	req->release();
}

void Room::smallTichuCallBack(HttpClient *sender, HttpResponse *response){
	std::vector<char> *buffer = response->getResponseData();
	std::string strRes = std::string(buffer->begin(), buffer->end());
	if (strRes.length() > 1000)
		return;
	std::string parsedRes = "";
	char *rest = NULL;
	char res[2000];
	strcpy(res, strRes.c_str());
	parsedRes = strtok(res, ",");
	std::string temp = parsedRes;

	
	int j = 0;
	char a[14];
	char b[14];
	char c[14];
	/*
	for (int i = 8; i < 14; i++){
		_card[i]->setCard(temp[j], temp[j + 1], temp[j + 2]);// new Card(temp[j], temp[j + 1], temp[j + 2]);
		j += 3;
	}*/

	for (int i = 0; i < 14; i++){
		if (i < 8){
			a[i] = _card[i]->cardToServer()[0];
			b[i] = _card[i]->cardToServer()[1];
			if (a[i] == '5') b[i] +=2;
			c[i] = _card[i]->cardToServer()[2];
		}
		else{
			a[i] = temp[j];
			b[i] = temp[j + 1];
			if (a[i] == '5') b[i] +=2;
			c[i] = temp[j + 2]; 
			j += 3;
		}

	}
	for (int i = 0; i < 14; i++){
		for (j = 0; j < 14; j++){
			if ((b[i] < b[j]) || ((b[i] == b[j]) && c[i] < c[j])){
				char temp = a[i];
				a[i] = a[j];
				a[j] = temp;

				temp = b[i];
				b[i] = b[j];
				b[j] = temp;

				temp = c[i];
				c[i] = c[j];
				c[j] = temp;
			}
		}
	}
	//bool chamsae = false;
	for (int i = 0; i < 14; i++){
		if (a[i] == '5') b[i] -=2;
		_card[i]->setCard(a[i], b[i], c[i]);//new Card(temp[j], temp[j + 1], temp[j + 2]);
		//if (_card[i]->getValue() == -10) chamsae = true;
	}

	for (int i = 0; i < 8; i++){
		card[i]->setTexture(Director::getInstance()->getTextureCache()->addImage(_card[i]->toString() + ".png"));
		
	}
	

	for (int i = 8; i < 14; i++)
	{
		addCard_user(winSize.width / 2, winSize.height / 2, 50 + i, i, _card[i]);
	}
	scoreBoard->setString("org : SmallTichu, red : Pass");
}

void Room::smallTichuCallBack2(HttpClient *sender, HttpResponse *response){
	std::vector<char> *buffer = response->getResponseData();
	std::string strRes = std::string(buffer->begin(), buffer->end());
	if (strRes.length() > 1000)
		return;
	std::string parsedRes = "";
	char *rest = NULL;
	char res[2000];
	strcpy(res, strRes.c_str());
	parsedRes = strtok(res, ",");
	std::string temp = parsedRes;
	int j = 0;

	
	for (int i = 0; i < 4; i++){
		if (temp[i] - '0' != 0) tichu[i] = 2;
	}
	g.tichuCall = 2;
	initBoard();
	initBorderNum();
	setNum();
	scoreBoard->setString("BeforeStartGame..");


	
}

void Room::smallTichu2(int call){
	scoreBoard->setString("Wait Other Player");

	std::string url = "http://bit.sparcs.org:12312/sTichu/";
	url += r;
	url += "/" + intToString(t);
	url += "/" + intToString(call);

	HttpRequest* req = new HttpRequest();
	req->setUrl(url.c_str());
	req->setRequestType(HttpRequest::Type::GET);
	req->setResponseCallback(this, httpresponse_selector(Room::smallTichuCallBack2));
	HttpClient::getInstance()->send(req);
	req->release();



}

void Room::initBoard(){
	for (int i = 0; i < 3; i++){
		
		boader[i] = Sprite::create("boader.png");
		float origX = boader[i]->getBoundingBox().size.width;
		float origY = boader[i]->getBoundingBox().size.height;

		boader[i]->setScaleX(cardX*1.1);
		boader[i]->setScaleY(cardY*1.1);

		float dx, dy;

		if (i == 0){
			dx = clip->getContentSize().width / 2;
			dy = clip->getContentSize().height / 2;
		}
		else if (i == 1){
			dx = clip->getContentSize().width / 2 - boader[i]->getBoundingBox().size.width - winSize.width / 20;
			dy = clip->getContentSize().height / 2;
		}
		else if (i == 2){
			dx = clip->getContentSize().width / 2 + boader[i]->getBoundingBox().size.width + winSize.width / 20;
			dy = clip->getContentSize().height / 2;
		}
		boader[i]->setAnchorPoint(ccp(0.5, 0.5));
		boader[i]->setPosition(ccp(dx, dy));
		clip->addChild(boader[i], 100);
	}
}


void Room::exchangeCard(){
	std::string url = "http://bit.sparcs.org:12312/exCard/";
	url += r;
	url += "/" + intToString(t) + "/";
	for (int i = 0; i < 3; i++){
		url += _card[exDepth[i] - 50]->cardToServer();
	}
	HttpRequest* req = new HttpRequest();
	req->setUrl(url.c_str());
	req->setRequestType(HttpRequest::Type::GET);
	req->setResponseCallback(this, httpresponse_selector(Room::exchangeCardCallBack));
	HttpClient::getInstance()->send(req);
	req->release();
	scoreBoard->setString("Waiting Other Player");
}

void Room::exchangeCardCallBack(HttpClient *sender, HttpResponse *response){
	std::vector<char> *buffer = response->getResponseData();
	std::string strRes = std::string(buffer->begin(), buffer->end());
	if (strRes.length() > 1000)
		return;
	std::string parsedRes = "";
	char *rest = NULL;
	char res[2000];
	strcpy(res, strRes.c_str());
	parsedRes = strtok(res, ",");
	std::string temp = parsedRes;
	int j = 0;
//	Sleep(3000);

	for (int i = 0; i < 3; i++){
		_card[exDepth[i] - 50]->setCard(temp[j], temp[j+1], temp[j+2]);
		j += 3;

		int actualDuration = 1;
		float dx, dy;
		dx = card[exDepth[i] - 50]->getPositionX();
		dy = card[exDepth[i] - 50]->getPositionY() - 500;

		
		//while (!exLock[i]);
		CCFiniteTimeAction *actionMove = CCMoveTo::create(actualDuration, ccp(dx, dy));
		CCFiniteTimeAction *deal = CCMoveTo::create(actualDuration, ccp(exCardX[i], exCardY[i]));
		card[exDepth[i] - 50]->runAction(CCSequence::create(actionMove, deal, NULL));
		card[exDepth[i] - 50]->setTexture(Director::getInstance()->getTextureCache()->addImage(_card[exDepth[i] - 50]->toString() + ".png"));

	}
	scoreBoard->setString("Start Game!");
	for (int i = 14; i < 56; i++)
	{
		addCard_other(winSize.width / 2, winSize.height / 2, 50 + i, i, _card[i]);
	}

	bool chamsae = false;
	for (int i = 0; i < 14; i++){
		if (_card[i]->getValue() == -10) chamsae = true;
	}
	if (chamsae) yesChamsae();
	else notChamsae();



}

void Room::floorCallBack(HttpClient *sender, HttpResponse *response){
	std::vector<char> *buffer = response->getResponseData();
	std::string strRes = std::string(buffer->begin(), buffer->end());
	if (strRes.length() > 1000)
		return;
	std::string parsedRes = "";
	char *rest = NULL;
	char res[2000];
	strcpy(res, strRes.c_str());
	parsedRes = strtok(res, ",");
	std::string temp = parsedRes;

	int b = (4 - t) % 4;
	int j = 0;
	b  =(b+g.curTurn) % 4;

	if (temp.length() == 1){
		passCall[b]->setVisible(true);
		if (g.pass()){
			cleanFloor();
		}
		else{
			g.prevTurn = g.curTurn;
			g.curTurn = g.nextTurn();
		}
		setTurn(g.curTurn);
		if (g.curTurn != t){
			floor();
		}
		scoreBoard->setString("Current Turn : " + intToString(g.curTurn));
		return;
	}
	passCall[b]->setVisible(false);
	float gap;

	if ((temp.length() / 3) >= 5){
		gap = 3 * card[0]->getBoundingBox().size.width;
	}
	else if ((temp.length() / 3) >= 3){
		gap = 2 * card[0]->getBoundingBox().size.width;
	}
	else{
		gap = 1 * card[0]->getBoundingBox().size.width;
	}
	if (b == 1){
		for (int i = 0; i < (temp.length() / 3); i++){
			float dx = winSize.width / 2;
			float dy = winSize.height / 20 * 13 + winSize.height / 30;
			int index = rand() % 14 + 14;
			while (_card[index]->getValue() != -1){
				index = rand() % 14 + 14;
			}
			current_floor[0][cfloor++] = card[index];
			//	curCard[cfloor++] = _card[index];

			_card[index]->setCard(temp[j], temp[j + 1], temp[j + 2]);
			_card[index]->setSubmit(true);


			dx += winSize.width / 4;
			dy += gap / (temp.length() / 3) * (i + 1) - gap / 2 - gap / (temp.length() / 3) / 2;

			CCFiniteTimeAction *actionMove = CCMoveTo::create(1, ccp(dx, dy));
			card[index]->runAction(CCSequence::create(actionMove, NULL));
			card[index]->setTexture(Director::getInstance()->getTextureCache()->addImage(_card[index]->toString() + ".png"));
			card[index]->setZOrder(50+i);
			j += 3;
		}
	}
	else if (b == 2){
		for (int i = 0; i < (temp.length() / 3); i++){
			float dx = winSize.width / 2;
			float dy = winSize.height / 30;
			int index = rand() % 14 + 28;
			while (_card[index]->getValue() != -1){
				index = rand() % 14 + 28;
			}
			current_floor[0][cfloor++] = card[index];
			//curCard[cfloor++] = _card[index];
			_card[index]->setCard(temp[j], temp[j + 1], temp[j + 2]);

			_card[index]->setSubmit(true);

			dx += gap / (temp.length() / 3) * (i + 1) - gap / 2 - gap / (temp.length() / 3) / 2;
			dy += winSize.height - winSize.height / 6;

			CCFiniteTimeAction *actionMove = CCMoveTo::create(1, ccp(dx, dy));
			card[index]->runAction(CCSequence::create(actionMove, NULL));
			card[index]->setTexture(Director::getInstance()->getTextureCache()->addImage(_card[index]->toString() + ".png"));
			card[index]->setZOrder(50 + i);
			j += 3;
		}

	}
	else if (b == 3){
		for (int i = 0; i < (temp.length() / 3); i++){
			float dx = winSize.width / 2;
			float dy = winSize.height / 20 * 13 + winSize.height / 30;
			int index = rand() % 14 + 42;
			while (_card[index]->getValue() != -1){
				index = rand() % 14 + 42;
			}
			current_floor[0][cfloor++] = card[index];
			//curCard[cfloor++] = _card[index];

			_card[index]->setSubmit(true);

			_card[index]->setCard(temp[j], temp[j + 1], temp[j + 2]);

			dx -= winSize.width / 4;
			dy += gap / (temp.length() / 3) * (i + 1) - gap / 2 - gap / (temp.length() / 3) / 2;

			CCFiniteTimeAction *actionMove = CCMoveTo::create(1, ccp(dx, dy));
			card[index]->runAction(CCSequence::create(actionMove, NULL));
			card[index]->setTexture(Director::getInstance()->getTextureCache()->addImage(_card[index]->toString() + ".png"));
			card[index]->setZOrder(50 + i);
			j += 3;
		}

	}
	g.submit(g.curTurn);
	if (g.userCnt == 2 && g.submitCheck){
		game_end();
		return;
	}
	for (int i = 0 + 14 * b; i < 14 * b + 14; i++)
	{
		if (_card[i]->getSubmit()){
			_card[i]->setSubmit(false);
			_card[i]->setCanUse(false);
			_card[i]->setFloor(true);
		}
	}
	if (g.curValue == 20){
		getD();
	}
	if (g.curValue == -10){
		getM();
	}

	g.prevTurn = g.curTurn;
	g.curTurn = g.nextTurn();
	if (temp[0] == '5' && temp[2] == '3'){
		g.curTurn = g.nextTurn();
	}

	setTurn(g.curTurn);
	if (g.curTurn != t){
		
		floor();
	}
	else{
	}
	scoreBoard->setString("Current Turn : " + intToString(g.curTurn));

}
void Room::floor(){

	std::string url = "http://bit.sparcs.org:12312/readyTurn/";
	url += r;
	url += "/";
	url += intToString(t);
	url += "/";
	url += intToString((g.prevTurn));

	HttpRequest* req = new HttpRequest();
	req->setUrl(url.c_str());
	req->setRequestType(HttpRequest::Type::GET);
	req->setResponseCallback(this, httpresponse_selector(Room::floorCallBack));
	HttpClient::getInstance()->send(req);
	req->release();
	scoreBoard->setString("Current Turn : " + intToString(g.curTurn));
	
}


void Room::submitCard(std::string cards, int f){
	pLabel->setVisible(false);

	std::string url = "http://bit.sparcs.org:12312/throw/";
	url += r + "/";
	url += intToString(t) + "/" + intToString((g.prevTurn));
	url += "/" + cards;
	url += "/" + intToString(f);
	HttpRequest* req = new HttpRequest();
	req->setUrl(url.c_str());
	req->setRequestType(HttpRequest::Type::GET);
	//req->setResponseCallback(this, httpresponse_selector(Room::exchangeCardCallBack));
	HttpClient::getInstance()->send(req);
	req->release();

	if (g.userCnt == 2 && g.submitCheck){
		game_end();
		return;
	}

	if (g.lastSubmit != -1){
		g.prevTurn = g.curTurn;
		g.curTurn = g.nextTurn();
	}
	
	if (cards[0] == '5' && cards[2] == '3'){
		g.curTurn = g.nextTurn();
	}

	setTurn(g.curTurn);
	scoreBoard->setString("Current Turn : " + intToString(g.curTurn));
	floor();
}


void Room::cleanFloor(){
	for (int i = 0; i < cfloor; i++)
	{
		int wRand = rand() % int(winSize.width / 6);
		wRand -= int(winSize.width / 12);
		int hRand = rand() % int(winSize.width / 6);
		hRand -= int(winSize.width / 12); 
		int angle = rand() % 360;
		int w = winSize.width / 2 + wRand;
		int h = winSize.height / 20 * 13 + winSize.height / 30 + hRand;
		auto rotate1 = RotateTo::create(0, angle);

		CCFiniteTimeAction *clean = CCMoveTo::create(1, ccp(w, h));
		CallFunc * a = CallFunc::create(CC_CALLBACK_0(Room::passCallsetVisibleFalse, this));
		auto spawn = Spawn::create(rotate1, clean, NULL);
		current_floor[0][i]->runAction(CCSequence::create(spawn, a, NULL));
	}
}

void Room::passCallsetVisibleFalse(){
	for (int i = 0; i < 4; i++){
		passCall[i]->setVisible(false);
	}
}

void Room::game_end(){
	int i;
	for (i = 0; i < 4; i++){
		g.users[i]->numAvailCard = 0;
	}
	caculate_score();


	scoreBoard->setString("End Game..");
	endGame = true;

	/*
	auto pScene = Room::createTable(t, r, score);
	Director::getInstance()->pushScene(pScene);*/
}

void Room::caculate_score(){
	if (g.users[0]->getRank() == 1 && g.users[2]->getRank() == 2){
		score[0] += 100;
		score[2] += 100;
		return;
	}
	else if (g.users[1]->getRank() == 1 && g.users[3]->getRank() == 2){
		score[1] += 100;
		score[3] += 100;
		return;
	}
	int tichuCnt = 0;
	for (int i = 0; i < 4; i++){
		if (g.users[i]->getRank() == 1 && tichu[i] != 0){
			if (tichu[i] == 1) score[i] += 200;
			else if (tichu[i] == 2) score[i] += 100;
			tichuCnt++;
		}
		else if (tichu[i] != 0){
			if (tichu[i] == 1) score[i] -= 200;
			else if (tichu[i] == 2) score[i] -= 100;
			tichuCnt++;
		}
	}
	if (tichuCnt != 0) return;

	int index = 0;
	int a;
	for (int i = 0; i < 4; i++){
		if (g.users[i]->getRank() == 4){
			a = g.score[i];
		}
		else{
			score[i] += g.score[i];
			if (g.users[i]->getRank() == 1) index = i;
		}
	}
	g.score[index] += a;
	return;


}

void Room::restart(){
	std::string url = "http://bit.sparcs.org:12312/restart/";
	url += r;
	url += "/";
	url += intToString(t);

	HttpRequest* req = new HttpRequest();
	req->setUrl(url.c_str());
	req->setRequestType(HttpRequest::Type::GET);
	req->setResponseCallback(this, httpresponse_selector(Room::restartCallBack));
	HttpClient::getInstance()->send(req);
	req->release();
	scoreBoard->setString("Restarting Game..");

}
void Room::restartCallBack(HttpClient *sender, HttpResponse *response){
	std::vector<char> *buffer = response->getResponseData();
	std::string strRes = std::string(buffer->begin(), buffer->end());
	if (strRes.length() > 1000)
		return;
	std::string parsedRes = "";
	char *rest = NULL;
	char res[2000];
	strcpy(res, strRes.c_str());
	parsedRes = strtok(res, ",");


	auto pScene = Room::createTable(t, r, score);
	Director::getInstance()->pushScene(pScene);
	

}

void Room::initBorderNum()
{
	borderFirstNum = LabelTTF::create("a", "Helvetica", 32);
	borderFirstNum->setPosition(clip->getContentSize().width / 2, clip->getContentSize().height / 2);
	borderFirstNum->setColor(Color3B(0, 0, 255));
	borderSecondNum = LabelTTF::create("b", "Helvetica", 32);
	borderSecondNum->setPosition(clip->getContentSize().width / 2 - boader[0]->getBoundingBox().size.width - winSize.width / 20, clip->getContentSize().height / 2);
	borderSecondNum->setColor(Color3B(0, 0, 255));
	borderThirdNum = LabelTTF::create("c", "Helvetica", 32);
	borderThirdNum->setPosition(clip->getContentSize().width / 2 + boader[0]->getBoundingBox().size.width + winSize.width / 20, clip->getContentSize().height / 2);
	borderThirdNum->setColor(Color3B(0, 0, 255));
	
	if (this->t == 0)
	{
		borderFirstNum->setString("1");
		borderSecondNum->setString("2");
		borderThirdNum->setString("3");
	}
	else if (this->t == 1)
	{
		borderFirstNum->setString("0");
		borderSecondNum->setString("2");
		borderThirdNum->setString("3");
	}
	else if (this->t == 2)
	{
		borderFirstNum->setString("0");
		borderSecondNum->setString("1");
		borderThirdNum->setString("3");
	}
	else if (this->t == 3)
	{
		borderFirstNum->setString("0");
		borderSecondNum->setString("1");
		borderThirdNum->setString("2");
	}
	
	clip->addChild(borderFirstNum, 300);
	clip->addChild(borderSecondNum, 300);
	clip->addChild(borderThirdNum, 300);
	
}
void Room::eraseBorderNum()
{
	borderFirstNum->setVisible(false);
	borderSecondNum->setVisible(false);
	borderThirdNum->setVisible(false);
}

void Room::setTurn(int turn){
	int b = (4 + turn - t) % 4;
	num0->setColor(Color3B(255, 255, 0));
	num1->setColor(Color3B(255, 255, 0));
	num2->setColor(Color3B(255, 255, 0));
	num3->setColor(Color3B(255, 255, 0));
	if (b == 0){
		num0->setColor(Color3B(255, 0, 0));
	}
	if (b == 1){
		num1->setColor(Color3B(255, 0, 0));
	}
	if (b == 2){
		num2->setColor(Color3B(255, 0, 0));
	}
	if (b == 3){
		num3->setColor(Color3B(255, 0, 0));
	}
}

void Room::setNum()
{
	num0 = LabelTTF::create("0", "Helvetica", 45);
	num0->setPosition(winSize.width / 2, winSize.height * 1 / 2 - winSize.height/15);
	num0->setColor(Color3B(255, 255, 0));
	num3 = LabelTTF::create("3", "Helvetica", 45);
	num3->setPosition(winSize.width / 7, winSize.height * 13 / 20 + winSize.height / 30);
	num3->setColor(Color3B(255, 255, 0));
	num2 = LabelTTF::create("2", "Helvetica", 45);
	num2->setPosition(winSize.width / 2, winSize.height * 37 / 40);
	num2->setColor(Color3B(255, 255, 0));
	num1 = LabelTTF::create("1", "Helvetica", 45);
	num1->setPosition(winSize.width * 6 / 7, winSize.height * 13 / 20 + winSize.height / 30);
	num1->setColor(Color3B(255, 255, 0));


	if (this->t == 0)
	{
	}
	else if (this->t == 1)
	{
		num0->setString("1");
		num1->setString("2");
		num2->setString("3");
		num3->setString("0");
	}
	else if (this->t == 2)
	{
		num0->setString("2");
		num1->setString("3");
		num2->setString("0");
		num3->setString("1");
	}
	else if (this->t == 3)
	{
		num0->setString("3");
		num1->setString("0");
		num2->setString("1");
		num3->setString("2");
	}

	this->addChild(num0, 100);
	this->addChild(num1, 100);
	this->addChild(num2, 100);
	this->addChild(num3, 100);
}

void Room::initsBoard(){
	sBoardClick = false;
	float w = winSize.width;
	float h = winSize.height;

	float dx = w / 2;
	float dy = winSize.height * 13 / 20 + winSize.height / 30;

	scBoard = Sprite::create("scoreBoard.png");
	float origX = scBoard->getBoundingBox().size.width;
	float origY = scBoard->getBoundingBox().size.height;

	scBoard->setScaleX(w / (15 * origX) * 14);
	scBoard->setScaleY(h / (30 * origY) * 13);


	scBoard->setAnchorPoint(ccp(0.5, 0.5));
	scBoard->setPosition(ccp(dx, dy));
	scBoard->setVisible(false);
	this->addChild(scBoard, 800);

	int i = 0;
	for (i = 0; i < 4; i++){
		writeScore[i] = LabelTTF::create("ABC", "Hevetica", 25);
		writeScore[i]->setColor(Color3B(0, 0, 0));
		writeScore[i]->setVisible(false);
		writeScore[i]->setPosition(w/2, h/4*3-h/8 + h/16*i);
		this->addChild(writeScore[i], 805);
	}
	
}
void Room::showScore(){
	scBoard->setVisible(true);
	int i;
	std::string s = "";
	for (i = 0; i < 4; i++){
		s = "";
		s += intToString(i) + ") ";
		s += "Score : " + intToString(g.score[i]) ;
		s += " TotalScore : " + intToString(score[i]);
		if (tichu[i] == 1){
			s += " largeTichu";
		}
		else if (tichu[i] == 2){
			s += " smallTichu";
		}
		writeScore[i]->setVisible(true);
		writeScore[i]->setString(s);
	}
}




void Room::selectD(int f)
{
	dBoard = Sprite::create("scoreBoard.png");
	float origX = dBoard->getBoundingBox().size.width;
	float origY = dBoard->getBoundingBox().size.height;

	dBoard->setScaleX(winSize.width / (15 * origX) * 14);
	dBoard->setScaleY(winSize.height / (30 * origY) * 13);
	dBoard->setPosition(winSize.width / 2, winSize.height / 2);
	dBoard->setVisible(true);
	this->addChild(dBoard, 900);
	if (g.curTurn != 0 && g.curTurn != 2){
		auto item0 = MenuItemFont::create(
			"To player 0",
			CC_CALLBACK_1(Room::submitCardD, this, f, 0));
		item0->setColor(Color3B(0, 0, 0));
		auto item2 = MenuItemFont::create(
			"To player 2",
			CC_CALLBACK_1(Room::submitCardD, this, f, 2));
		item2->setColor(Color3B(0, 0, 0));
		// 메뉴 생성
		dMenu = Menu::create(item0, item2, NULL);
		dMenu->setPosition(winSize.width / 2, winSize.height / 2);
	}
	else if (g.curTurn != 1 && g.curTurn != 3){
		auto item1 = MenuItemFont::create(
			"To player 1",
			CC_CALLBACK_1(Room::submitCardD, this, f, 1));
		item1->setColor(Color3B(0, 0, 0));

		auto item3 = MenuItemFont::create(
			"To player 3",
			CC_CALLBACK_1(Room::submitCardD, this, f, 3));
		item3->setColor(Color3B(0, 0, 0));
		// 메뉴 생성
		dMenu = Menu::create(item1, item3, NULL);
		dMenu->setPosition(winSize.width / 2, winSize.height / 2);
	}
	// 메뉴 정렬
	dMenu->alignItemsVertically();
	// 레이어에 메뉴 객체 추가
	this->addChild(dMenu, 905);
}


void Room::getD()
{
	std::string url = "http://bit.sparcs.org:12312/getD/";
	url += r + "/";
	HttpRequest* req = new HttpRequest();
	req->setUrl(url.c_str());
	req->setRequestType(HttpRequest::Type::GET);
	req->setResponseCallback(this, httpresponse_selector(Room::getDCallBack));
	HttpClient::getInstance()->send(req);
	req->release();
}
void Room::getDCallBack(HttpClient *sender, HttpResponse *response)
{
	std::vector<char> *buffer = response->getResponseData();
	std::string strRes = std::string(buffer->begin(), buffer->end());
	if (strRes.length() > 1000)
		return;
	std::string parsedRes = "";
	char *rest = NULL;
	char res[2000];
	strcpy(res, strRes.c_str());
	parsedRes = strtok(res, ",");
	g.Dra = atoi(parsedRes.c_str());
}
void Room::submitCardD(Object* pSender, int f, int D)
{
	g.Dra = D;
	this->removeChild(dMenu);
	this->removeChild(dBoard);
	std::string url = "http://bit.sparcs.org:12312/throwD/";
	url += r + "/";
	url += intToString(t) + "/" + intToString((g.prevTurn));
	std::string c = "504";
	url += "/" + c;//용카드//TODO: 용카드 번호가 뭔가요
	url += "/" + intToString(f);
	url += "/" + intToString(D);
	HttpRequest* req = new HttpRequest();
	req->setUrl(url.c_str());
	req->setRequestType(HttpRequest::Type::GET);
	//req->setResponseCallback(this, httpresponse_selector(Room::exchangeCardCallBack));
	HttpClient::getInstance()->send(req);
	req->release();

	if (g.userCnt == 2 && g.submitCheck){
		game_end();
		return;
	}

	g.prevTurn = g.curTurn;
	g.curTurn = g.nextTurn();
	setTurn(g.curTurn);
	scoreBoard->setString("Current Turn : " + intToString(g.curTurn));
	floor();
}



void Room::yesChamsae()
{
	std::string url = "http://bit.sparcs.org:12312/imChamsae/";
	url += r;
	url += "/" + intToString(t);

	HttpRequest* req = new HttpRequest();
	req->setUrl(url.c_str());
	req->setRequestType(HttpRequest::Type::GET);
	req->setResponseCallback(this, httpresponse_selector(Room::writeMahjong));
	HttpClient::getInstance()->send(req);
	req->release();
}


void Room::notChamsae()
{
	std::string url = "http://bit.sparcs.org:12312/notChamsae/";
	url += r;
	url += "/" + intToString(t);

	HttpRequest* req = new HttpRequest();
	req->setUrl(url.c_str());
	req->setRequestType(HttpRequest::Type::GET);
	req->setResponseCallback(this, httpresponse_selector(Room::writeMahjong));
	HttpClient::getInstance()->send(req);
	req->release();
}


void Room::writeMahjong(HttpClient *sender, HttpResponse *response)
{
	std::vector<char> *buffer = response->getResponseData();
	std::string strRes = std::string(buffer->begin(), buffer->end());
	if (strRes.length() > 1000)
		return;
	std::string parsedRes = "";
	char *rest = NULL;
	char res[2000];
	strcpy(res, strRes.c_str());
	parsedRes = strtok(res, ",");
	std::string temp = parsedRes;
	chamSae = atoi(temp.c_str());


	g.curTurn = chamSae;
	g.prevTurn = (g.curTurn + 3) % 4;

	g.gameStart = true;
	scoreBoard->setVisible(false);
	setTurn(g.curTurn);
	if (g.curTurn == t){

	}
	else{
		floor();
	}
}

void Room::selectM(int f)
{
	mBoard = Sprite::create("scoreBoard.png");
	float origX = mBoard->getBoundingBox().size.width;
	float origY = mBoard->getBoundingBox().size.height;

	mBoard->setScaleX(winSize.width / (15 * origX) * 14);
	mBoard->setScaleY(winSize.height / (3 * origY) * 2);
	mBoard->setPosition(winSize.width / 2, winSize.height / 2);
	mBoard->setVisible(true);
	this->addChild(mBoard, 900);

	auto item1 = MenuItemFont::create(
		"1",
		CC_CALLBACK_1(Room::submitM, this, f, 1));
	item1->setColor(Color3B(0, 0, 0));

	auto item2 = MenuItemFont::create(
		"2",
		CC_CALLBACK_1(Room::submitM, this, f, 2));
	item2->setColor(Color3B(0, 0, 0));

	auto item3 = MenuItemFont::create(
		"3",
		CC_CALLBACK_1(Room::submitM, this, f, 3));
	item3->setColor(Color3B(0, 0, 0));

	auto item4 = MenuItemFont::create(
		"4",
		CC_CALLBACK_1(Room::submitM, this, f, 4));
	item4->setColor(Color3B(0, 0, 0));

	auto item5 = MenuItemFont::create(
		"5",
		CC_CALLBACK_1(Room::submitM, this, f, 5));
	item5->setColor(Color3B(0, 0, 0));

	auto item6 = MenuItemFont::create(
		"6",
		CC_CALLBACK_1(Room::submitM, this, f, 6));
	item6->setColor(Color3B(0, 0, 0));

	auto item7 = MenuItemFont::create(
		"7",
		CC_CALLBACK_1(Room::submitM, this, f, 7));
	item7->setColor(Color3B(0, 0, 0));
	auto item8 = MenuItemFont::create(
		"8",
		CC_CALLBACK_1(Room::submitM, this, f, 8));
	item8->setColor(Color3B(0, 0, 0));
	auto item9 = MenuItemFont::create(
		"9",
		CC_CALLBACK_1(Room::submitM, this, f, 9));
	item9->setColor(Color3B(0, 0, 0));
	auto item10 = MenuItemFont::create(
		"10",
		CC_CALLBACK_1(Room::submitM, this, f, 10));
	item10->setColor(Color3B(0, 0, 0));
	auto item11 = MenuItemFont::create(
		"11",
		CC_CALLBACK_1(Room::submitM, this, f, 11));
	item11->setColor(Color3B(0, 0, 0));
	auto item12 = MenuItemFont::create(
		"12",
		CC_CALLBACK_1(Room::submitM, this, f, 12));
	item12->setColor(Color3B(0, 0, 0));
	auto item13 = MenuItemFont::create(
		"13",
		CC_CALLBACK_1(Room::submitM, this, f, 13));
	item13->setColor(Color3B(0, 0, 0));
	// 메뉴 생성d
	mMenu = Menu::create(item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11, item12, item13, NULL);
	mMenu->setPosition(winSize.width / 2, winSize.height /2);

	// 메뉴 정렬
	mMenu->alignItemsVertically();
	// 레이어에 메뉴 객체 추가
	this->addChild(mMenu, 905);
}

void Room::submitM(Object* pSender, int f, int M)
{
	g.Maj = M;
	this->removeChild(mMenu);
	this->removeChild(mBoard);
	std::string url = "http://bit.sparcs.org:12312/throwM/";
	url += r + "/";
	url += intToString(t) + "/" + intToString((g.prevTurn));
	std::string c = "501";
	url += "/" + c;//TODO: 참새 번호가 뭔가요
	url += "/" + intToString(f);
	url += "/" + intToString(M);
	HttpRequest* req = new HttpRequest();
	req->setUrl(url.c_str());
	req->setRequestType(HttpRequest::Type::GET);
	//req->setResponseCallback(this, httpresponse_selector(Room::exchangeCardCallBack));
	HttpClient::getInstance()->send(req);
	req->release();

	if (g.userCnt == 2 && g.submitCheck){
		game_end();
		return;
	}
	g.prevTurn = g.curTurn;
	g.curTurn = g.nextTurn();
	setTurn(g.curTurn);
	scoreBoard->setString("Current Turn : " + intToString(g.curTurn));
	floor();
}

void Room::getM()
{
	std::string url = "http://bit.sparcs.org:12312/getM/";
	url += r + "/";
	HttpRequest* req = new HttpRequest();
	req->setUrl(url.c_str());
	req->setRequestType(HttpRequest::Type::GET);
	req->setResponseCallback(this, httpresponse_selector(Room::getMCallBack));
	HttpClient::getInstance()->send(req);
	req->release();
}

void Room::getMCallBack(HttpClient *sender, HttpResponse *response)
{
	std::vector<char> *buffer = response->getResponseData();
	std::string strRes = std::string(buffer->begin(), buffer->end());
	if (strRes.length() > 1000)
		return;
	std::string parsedRes = "";
	char *rest = NULL;
	char res[2000];
	strcpy(res, strRes.c_str());
	parsedRes = strtok(res, ",");
	g.Maj = atoi(parsedRes.c_str());

	pLabel->setString("Must Submit Value " + parsedRes);
	pLabel->setVisible(true);
}