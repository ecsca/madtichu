#include "Table.h"


USING_NS_CC;

Scene* Table::createTable()
{
	auto scene = Scene::create();
	auto layer = Table::create();
	

	scene->addChild(layer, 0);

	return scene;
}

bool Table::init()
{

	winSize = CCDirector::sharedDirector()->getWinSize();

	clip = CCClippingNode::create();
	clip->setAnchorPoint(ccp(0.5, 0.5));
	
	clip->setContentSize(Size(winSize.width - 30, winSize.height / 2 -30));
	fixY = 30;
	fixX = 15;

	CCDrawNode* shape = CCDrawNode::create();
	CCPoint pts[8];
	pts[0] = ccp(0, 0);
	pts[1] = ccp(clip->getContentSize().width, 0);
	pts[2] = ccp(clip->getContentSize().width, clip->getContentSize().height);
	pts[3] = ccp(80, clip->getContentSize().height);
	pts[4] = ccp(80, clip->getContentSize().height+30);
	pts[5] = ccp(0, clip->getContentSize().height+30);
	pts[6] = ccp(50, clip->getContentSize().height);
	pts[7] = ccp(0, clip->getContentSize().height);
	shape->drawPolygon(pts, 8, ccc4f(1, 1, 1, 1), 0, ccc4f(1, 1, 1, 1));

	clip->setStencil(shape);
	this->addChild(clip, 2);
	clip->setPosition(ccp(winSize.width / 2, winSize.height / 2 - clip->getContentSize().height / 2));
	
	CCSprite * bg = CCSprite::create("woodBackground_hand.jpg");
	bg->setAnchorPoint(ccp(0.5, 0.5));
	bg->setPosition(ccp(clip->getContentSize().width / 2, clip->getContentSize().height / 2));
	clip->addChild(bg, 3);

	/*
	card[0] = Sprite::create("Back.png");
	clip->addChild(card[0], 100);
	//card[0]->setAnchorPoint(ccp(0.5, 0.5));
	card[0]->setPosition(0,0);*/
	

	if (!Layer::init())
	{
		return false;
	}
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Point origin = Director::getInstance()->getVisibleOrigin();
	//this->setColor(Color3B(255, 0, 0));

	initBg();
	initListener();
	initGame();
	return true;
}

void Table::initBg()
{
	CCSprite * bg = CCSprite::create("woodBackground.jpg");
	bg->setPosition(winSize.width/2, winSize.height/2);
	this->addChild(bg, 0);
}

void Table::initGame()
{
	int i;
	int width = winSize.width;
	int height = winSize.height;
	
	/* init values */

	depth = 100;
	current_card = NULL;
	cfloor = 0;

	
	g = GameHelper();
	memcpy(_card, g._card, sizeof(_card));

	
	for (int i = 0; i < 56; i++)
	{
		int xpos = rand() % width;
		int ypos = rand() % height;
		addCard(xpos, ypos, 50 + i, i, _card[i]);
	}
	
	submit = Sprite::create("submit.png");
	submit->setPosition(ccp(width, 0));
	submit->setAnchorPoint(ccp(1, 0));
	this->addChild(submit, width);

	
	pass = Sprite::create("pass.png");
	pass->setPosition(ccp(width-80, 0));
	pass->setAnchorPoint(ccp(1, 0));
	this->addChild(pass, width);

	pLabel = LabelTTF::create("ABC", "Hevetica", 32);
	pLabel->setPosition(Point(width/2, height/2+120));
	pLabel->setColor(Color3B(255, 255, 0));
	this->addChild(pLabel);

	scoreBoard = LabelTTF::create("ABC", "Hevetica", 32);
	scoreBoard->setPosition(Point(width / 2, height / 2 + 80));
	scoreBoard->setColor(Color3B(255, 255, 0));
	this->addChild(scoreBoard);

	/*
	hole = Sprite::create("hole.png");
	hole->setPosition(Point(width / 2 - 200, height / 2+15));*/
	holeX = 55;
	holeY = height / 2 + 15;
	//this->addChild(hole);

}

void Table::addCard(int x, int y, int z, int num, Card* _card)
{
	card[num] = Sprite::create("Back.png");
	
	//card[num] = Sprite::create("/cardImage/Back.png");
	card[num]->setPosition(ccp(x, y));
	card[num]->setAnchorPoint(ccp(0.5, 0.5));

	if (num < 14){
		clip->addChild(card[num], z);
	}
	else{
		this->addChild(card[num], z);
	}
	

	int actualDuration = 1;

	CCFiniteTimeAction *actionMove = CCMoveTo::create(actualDuration, ccp(winSize.width/2 + 0.5*num, winSize.height/2+0.5*num));

	//card[num]->runAction(CCSequence::create(actionMove, NULL, NULL));

	if (num < 14)
	{
		card[num]->setTexture(Director::getInstance()->getTextureCache()->addImage(_card->toString() + ".png"));
		CCFiniteTimeAction *deal = CCMoveTo::create(actualDuration, ccp(winSize.width / 2 + (num - 6)*(card[num]->getContentSize().width - 75) + card[num]->getContentSize().width / 2 * (num - 6), 0 + card[num]->getContentSize().height / 2 + 3));
		card[num]->runAction(CCSequence::create(actionMove, deal, NULL));
	}

	else if (num < 28)
	{
		auto rotate1 = RotateTo::create(0.1, -90);
		CCFiniteTimeAction *deal = CCMoveTo::create(actualDuration, ccp(winSize.width - card[num]->getContentSize().height / 2 - 3, winSize.height / 2 + (num - 27-5) * (card[num]->getContentSize().width - 100)));
		card[num]->runAction(CCSequence::create(actionMove, rotate1, deal, NULL));
	}
	else if (num<42)
	{
		CCFiniteTimeAction *deal = CCMoveTo::create(actualDuration, ccp(winSize.width / 2 + (num - 34)*(card[num]->getContentSize().width - 75) + card[num]->getContentSize().width / 2 * (num - 34), winSize.height - card[num]->getContentSize().height / 2 - 3));
		card[num]->runAction(CCSequence::create(actionMove, deal, NULL));
		
	}
	else
	{
		auto rotate = RotateTo::create(0.1, 90);
		CCFiniteTimeAction *deal = CCMoveTo::create(actualDuration, ccp(card[num]->getContentSize().height / 2 + 3, winSize.height / 2 + (num - 55 -5) * (card[num]->getContentSize().width - 100)));
		card[num]->runAction(CCSequence::create(actionMove, rotate, deal, NULL));
		
	}

}

void Table::initListener()
{
	auto listener = EventListenerTouchOneByOne::create();

	listener->onTouchBegan = CC_CALLBACK_2(Table::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(Table::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(Table::onTouchEnded, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void Table::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event)
{
	
	auto touchPoint = touch->getLocation();
	if (current_card == NULL) return;

	bool bTouch = current_card->getBoundingBox().containsPoint(touchPoint);

	if (bTouch)
	{
		current_card->setPosition(touchPoint.x-fixX, touchPoint.y-fixY);
	}


}

void Table::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event){
	current_card = NULL;
}
bool Table::onTouchBegan(Touch* touch, Event* event)
{
	auto touchPoint = touch->getLocation();
	int dx[4] = { 0, 30, 0, -30 };
	int dy[4] = { -30, 0, 30, 0 };
	Point realTouch = Point(touchPoint.x-fixX, touchPoint.y-fixY);

	for (int i = 55; i >= 0; i--){
		if (card[i]->getBoundingBox().containsPoint(realTouch)){
			if (!_card[i]->canUse()) return false;
			current_card = card[i];
			card[i]->setZOrder(depth++);

			if (_card[i]->getSubmit()){
				//card[i]->setPosition(ccp(card[i]->getPosition().x + dx[i / 14], card[i]->getPosition().y + dy[i / 14]));
				_card[i]->setSubmit(false);
				card[i]->setColor(Color3B(255, 255, 255));
				
				return true;
			}
			else{
				//card[i]->setPosition(ccp(card[i]->getPosition().x - dx[i / 14], card[i]->getPosition().y - dy[i / 14]));
				_card[i]->setSubmit(true);
				card[i]->setColor(Color3B(255, 255, 0));
				return true;
			}
		};
	}

	
	if (submit->getBoundingBox().containsPoint(touchPoint)){
		int userNum = g.getCurTurn();
		if (g.submit(userNum)){
			int s = cfloor;
			for (int i = 13+ 14*userNum; i >= 14*userNum; i--){
				if (_card[i]->getSubmit() && _card[i]->canUse()){
					_card[i]->setFloor(true);
					_card[i]->setSubmit(false);
					_card[i]->setCanUse(false);

					int actualDuration = 1;
					CCFiniteTimeAction *actionMove = CCMoveTo::create(actualDuration, ccp(holeX-fixX, holeY-fixY-50));
					CCFiniteTimeAction *deal = CCMoveTo::create(actualDuration, ccp(holeX, winSize.height));
					card[i]->runAction(CCSequence::create(actionMove, deal, NULL));
				
					//clip->removeChild(card[i]);
					
					cocos2d::Sprite* temp = Sprite::create(_card[i]->toString() + ".png");
					this->addChild(temp);
					current_floor[cfloor++] = temp;
				}
			}
			float gap;

			float x = winSize.width/2;
			float y = winSize.height / 2 + 200;
			if (cfloor - s >= 5){
				gap = 5 * card[0]->getBoundingBox().size.width;
			}
			else{
				gap = (cfloor - s) * card[0]->getBoundingBox().size.width;
			}
			for (int i = 1; i  <= (cfloor-s); i++){
				float dx = (gap / (cfloor - s) * i) - (gap / 2) - card[0]->getBoundingBox().size.width/2;
				current_floor[s + i - 1]->setPosition(ccp(x + dx, y));
			}
			pLabel->setString(g.toString());
		}
		
		else{
			for (int i = 13; i >= 0; i--){
				if (_card[i]->getSubmit() && _card[i]->canUse()){
					_card[i]->setSubmit(false);
					card[i]->setColor(Color3B(255, 255, 255));
				//	card[i]->setPosition(ccp(card[i]->getPosition().x + dx[userNum], card[i]->getPosition().y + dy[userNum]));
				}
			}
			pLabel->setString(g.toString());
		}

		
	}

	
	if (pass->getBoundingBox().containsPoint(touchPoint)){
		if (g.pass()){
			int actualDuration = 1;
			for (int i = 0; i < MAXCARD; i++){
				if (_card[i]->getFloor()){
					_card[i]->setFloor(false);;

				}
				
			}
			for (int i = 0; i < cfloor; i++){
				this->removeChild(current_floor[i]);
			}
			scoreBoard->setString(g.toString_score());
		}
		pLabel->setString(g.toString());
	}

	return true;
}

/*
void Table::onTouchesBegan(CCSet* pTouches, CCEvent *pEvent){
	CCSetIterator iter = pTouches->begin();
	CCTouch* pTouch = (CCTouch *)(*iter);
	CCPoint touchPoint = pTouch->getLocation();
	for (int i = 13; i >0; i++){
		CCRect rect = card[i]->boundingBox();
		if (rect.containsPoint(touchPoint)){
			card[i]->setPosition(ccp(0, 10));
			return;
		}
	}
}*/
