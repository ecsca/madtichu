#include "Card.h"
Suit Card::getSuit(){
	return suit;
}
int Card::getValue(){
	return value;
}
void Card::setSubmit(bool _submit){
	submit = _submit;
}
bool Card::getSubmit(){
	return submit;
}
bool Card::canUse(){
	return use;
}
void Card::setCanUse(bool _canUse){
	use = _canUse;
}